<?php



class Extended
{

    function addWaterMarkAndText($original_video_file_path, $filename)
    {

        $fileName = $filename;
        $font_name = 'roboto-bold.ttf';
        $font_path = FCPATH . 'font/' . $font_name;
        $watermark = FCPATH . 'assets/watermark_image/water-mark.png';
        $original_video_file_path;


        $folder = 'assets/userdata';

        $video_with_watermark = FCPATH . $folder . '/' . $fileName;
        if (!file_exists($folder)) {

            mkdir($folder, 0777, true);
        }

        // windows
        $cmd =  'C:\FFmpeg\bin\ffmpeg.exe  -i ' . $original_video_file_path . ' -i ' . $watermark . ' -filter_complex [1]scale=iw/15:-1[wm];[0][wm]overlay=x=main_w-overlay_w-10:y=main_h-overlay_h-10 ' .  $video_with_watermark . '';


        // klinux
        // $cmd = '/usr/bin/ffmpeg -i ' . $original_video_file_path . ' -i ' . $watermark . ' -filter_complex "[1]scale=iw/15:-1[wm];[0][wm]overlay=x=main_w-overlay_w-10:y=main_h-overlay_h-10" ' .  $video_with_watermark . '';


        $commands = exec($cmd);

        return $video_with_watermark;
    }
    function addWaterMarkToImage($original_image_file_path, $filename)
    {
        $fileName = $filename;
        $font_name = 'roboto-bold.ttf';
        $font_path = FCPATH . 'font/' . $font_name;
        $watermark = FCPATH . 'assets/watermark_image/water-mark.png';
        $original_image_file_path;


        $folder = 'assets/userdata';

        $image_with_watermark = FCPATH . $folder . '/' . $fileName;
        if (!file_exists($folder)) {

            mkdir($folder, 0777, true);
        }
        // windows
        $cmd =  'C:\FFmpeg\bin\ffmpeg.exe -i  ' . $original_image_file_path . ' -i ' . $watermark . ' -filter_complex [1]scale=iw/15:-1[wm];[0][wm]overlay=x=main_w-overlay_w-10:y=main_h-overlay_h-10 ' .  $image_with_watermark . '';





        // klinux
        // $cmd = '/usr/bin/ffmpeg -i ' . $original_image_file_path . ' -i ' . $watermark . ' -filter_complex "[1]scale=iw/15:-1[wm];[0][wm]overlay=x=main_w-overlay_w-10:y=main_h-overlay_h-10" ' .  $image_with_watermark . '';

        $commands = exec($cmd);
        return $image_with_watermark;
    }
}
