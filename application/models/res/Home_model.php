<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function addNewresturant($resturant, $uname, $password, $lon, $lat)
    {
        $this->db->insert('restaurants', $resturant);
        $resid = $this->db->insert_id();
        if ($resid) {
            $login['partners_id'] =  $resid;
            $login['username'] = $uname;
            $login['password'] =  md5($password);
            $login['partner_type'] =  2;
            $login['longitude'] =  $lon;
            $login['latitude'] =  $lat;
            $this->db->insert('partners_login', $login);
            $log_id = $this->db->insert_id();
            if ($log_id) {
                return true;
            } else {
                $this->db->where('restaurant_id', $resid)->delete('restaurants');
                return false;
            }
        } else {
            return false;
        }
    }

    public function getAllItemStoAddNewFood()
    {
        $data = [];
        $data['tax'] =  $this->db->where('is_active', 1)->get('food_tax')->result();
        $data['food_category'] =  $this->db->where('is_active', 1)->get('food_category')->result();
        $data['food_groups'] =  $this->db->where('is_active', 1)->get('food_groups')->result();
        return  $data;
    }
    public function getAllFudCats()
    {
        $ar = [];
        $results = $this->db->where('is_active', 1)->get('food_groups')->result();
        if ($results) {
            foreach ($results as $k => $vl) {
                $cats = new stdClass();
                $cats->food_group_name = $vl->food_group_name;
                $cats->foodgroupId = $vl->foodgroupId;
                $cats->catTimings = $this->getCatTimingsById($vl->foodgroupId, $_SESSION['uid']);
                $ar[] = $cats;
            }
            return $ar;
        }
    }

    function getCatTimingsById($fdcatId, $ResId)
    {
        return  $this->db->select('fud_timing.*')->where('restaurant_id', $ResId)->where('group_id', $fdcatId)->get('fud_timing')->row();
    }
    public function adNewFudItem($fud)
    {
        $this->db->insert('food_master', $fud);

        $log_id = $this->db->insert_id();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function getnumOfRecords()
    {
        $id = $_SESSION['uid'];
        return    $this->db->select('food_master.food_cat_id,food_master.restaurant_id,food_category.*')->join('food_category', 'food_category.food_category_id = food_master.food_cat_id', 'inner')->where('food_master.restaurant_id', $id)->where('food_master.is_deleted', 0)->group_by('food_category.food_category_id')->get('food_master')->num_rows();
    }
    public function getnumOfRecordsSearch($fudname)
    {
        $id = $_SESSION['uid'];
        return    $this->db->select('food_master.food_cat_id,food_master.restaurant_id,food_category.*')->join('food_category', 'food_category.food_category_id = food_master.food_cat_id', 'inner')->where('food_master.restaurant_id', $id)->where('food_master.is_deleted', 0)->like('food_master.item_name', $fudname)->group_by('food_category.food_category_id')->get('food_master')->num_rows();
    }
    public function ourMenuSearch($id, $fudname, $limit = null, $offset = null)
    {
        $cats = $this->db->select('food_master.food_cat_id,food_master.restaurant_id,food_category.*')->join('food_category', 'food_category.food_category_id = food_master.food_cat_id', 'inner')->where('food_master.restaurant_id', $id)->where('food_master.is_deleted', 0)->like('food_master.item_name', $fudname)->group_by('food_category.food_category_id')->order_by(' food_category.food_category_name', 'asc')->limit($limit)->offset($offset)->get('food_master')->result();



        // $cats =  $this->db->query("SELECT `food_master`.`food_cat_id`, `food_master`.`restaurant_id`, `food_category`.* FROM `food_master` INNER JOIN `food_category` ON `food_category`.`food_category_id` = `food_master`.`food_cat_id` WHERE `food_master`.`restaurant_id` = $id AND `food_master`.`is_deleted` = 0 AND `food_master`.`item_name` LIKE %$fudname%  GROUP BY `food_category`.`food_category_id` ORDER BY `food_category`.`food_category_name` ASC")->result();

        if ($cats) {
            $foods = [];
            foreach ($cats as $key => $val) {
                $foodlist = new stdClass();
                $foodlist->category = $val->food_category_name;
                $foodlist->food_category_id = $val->food_category_id;
                $foodlist->foodscat = $this->getallMenus($id, $val->food_category_id);

                $foods[] =  $foodlist;
            }
            return $foods;
        } else {
            return false;
        }
    }
    public function getMenuItems($limit, $offset)
    {


        $id = $_SESSION['uid'];


        $cats = $this->db->select('food_master.food_cat_id,food_master.restaurant_id,food_category.*')->join('food_category', 'food_category.food_category_id = food_master.food_cat_id', 'inner')->where('food_master.restaurant_id', $id)->where('food_master.is_deleted', 0)->group_by('food_category.food_category_id')->order_by(' food_category.food_category_name', 'asc')->limit($limit)->offset($offset)->get('food_master')->result();


        if ($cats) {
            $foods = [];
            foreach ($cats as $key => $val) {
                $foodlist = new stdClass();
                $foodlist->category = $val->food_category_name;
                $foodlist->food_category_id = $val->food_category_id;
                $foodlist->foodscat = $this->getallMenus($id, $val->food_category_id);

                $foods[] =  $foodlist;
            }
            return $foods;
        } else {
            return false;
        }
    }
    function getallMenus($rid, $catId)
    {
        return  $this->db->where('food_cat_id', $catId)->where('restaurant_id', $rid)->where('is_deleted', 0)->order_by('food_master.item_name', 'asc')->get('food_master')->result();
    }
    public function getfudsById($id)
    {
        return   $this->db->where('master_id', $id)->where('is_active', 1)->where('is_deleted', 0)->get('food_master')->row();
    }
    public function updateFudItems($fud, $mainId)
    {
        $this->db->where('master_id', $mainId)->update('food_master', $fud);
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function getUnitTypes()
    {
        return  $this->db->where('is_active', 1)->where('is_deleted', 0)->get('units')->result();
    }
    public function addnewunits($unt)
    {
        $this->db->insert('food_units', $unt);
        $log_id = $this->db->insert_id();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function getFoodvariants($id)
    {
        $restid =  $_SESSION['uid'];
        return  $this->db->where('restaurant_id', $restid)->where('master_id', $id)->where('master_id', $id)->where('is_deleted', 0)->get('food_units')->result();;
    }
    public function updateofferstatus($masterId, $offerstatus)
    {

        $uid = $this->db->select('activate_offer')->where('unit_id', $masterId)->get('food_units')->row();
        if ($uid->activate_offer == 2) {
            $fds['activate_offer'] = 1;
        } else {
            $fds['activate_offer'] = 2;
        }
        $this->db->where('unit_id', $masterId)->update('food_units', $fds);

        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function updatevariantstatus($masterId, $offerstatus)
    {

        $uid = $this->db->select('status')->where('unit_id', $masterId)->get('food_units')->row();
        if ($uid->status == 0) {
            $fds['status'] = 1;
        } else {
            $fds['status'] = 0;
        }
        $this->db->where('unit_id', $masterId)->update('food_units', $fds);

        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function getuntstoedit($masterId)
    {
        $arr = [];
        $arr['singlerow'] =   $uid = $this->db->where('unit_id', $masterId)->get('food_units')->row();
        $arr['result'] =  $this->db->where('is_active', 1)->where('is_deleted', 0)->get('units')->result();
        return $arr;
    }
    public function updateoldunit($dta, $mainId)
    {
        $this->db->where('unit_id', $mainId)->update('food_units', $dta);

        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }

    public function deleteunits($mainId)
    {
        $dta['is_deleted'] = 1;
        $this->db->where('unit_id', $mainId)->update('food_units', $dta);

        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function changerestfusstatus($mainId)
    {
        $result =  $this->db->select('restaurant_id,restaurant_status')->where('master_id', $mainId)->get('food_master')->row();
        $sts =  $this->db->select('restaurant_id')->where('master_id', $mainId)->get('food_units')->row();

        if ($sts) {
            if ($result->restaurant_status == 0) {
                $dta['restaurant_status'] = 1;
            } else {
                $dta['restaurant_status'] = 0;
            }
            $this->db->where('master_id', $mainId)->update('food_master', $dta);

            $log_id = $this->db->affected_rows();
            if ($log_id) {
                return 'Status changed successfully';
            } else {
                return 'failed to update status';
            }
        } else {
            return 'add fud variant to manage status';
        }
    }
    public function updateresttimingStatus($catid, $restid, $sttime, $endtime)
    {
        $tms['group_id'] = $catid;
        $tms['restaurant_id'] = $restid;
        $tms['starting_time'] = $sttime;
        $tms['ending_time'] = $endtime;

        $result =  $this->db->select('timing_id')->where('group_id', $catid)->where('restaurant_id', $restid)->get('fud_timing')->row();
        if ($result) {

            $this->db->where('timing_id', $result->timing_id)->update('fud_timing', $tms);
            $log_id = $this->db->affected_rows();
            if ($log_id) {
                return 'Timing updated successfully';
            } else {
                return 'failed to update Timing';
            }
        } else {
            $this->db->insert('fud_timing', $tms);
            $resid = $this->db->insert_id();
            if ($resid) {
                return 'Timing added successfully';
            } else {
                return 'failed to add timing';
            }
        }
    }
    public function addNewDistances($dis)
    {
        $this->db->insert('deliverycharge_distance', $dis);
        $resid = $this->db->insert_id();
        if ($resid) {
            return true;
        } else {
            return false;
        }
    }
    public function getAllDistances()
    {
        $resid =  $_SESSION['uid'];
        return  $this->db->where('is_active', 1)->where('restaurant_id',  $resid)->where('is_deleted', 0)->get('deliverycharge_distance')->result();
    }
    public function updateNewDistances($dis, $_mianId)
    {

        $this->db->where('id', $_mianId)->update('deliverycharge_distance', $dis);
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        } else {
            return false;
        }
    }

    public function getRestSettings()
    {
        $resid =  $_SESSION['uid'];
        return  $this->db->where('is_active', 1)->where('restaurant_id',  $resid)->get('settings')->row();
    }

    public function getResOnline()
    {
        $resid =  $_SESSION['uid'];
        return  $this->db->where('partners_id', $resid)->where('partner_type', 2)->get('partners_login')->row();;
    }

    public function newsettingsAdding($smainid, $settings)
    {
        if ($smainid) {
            $this->db->where('settings_id', $smainid)->update('settings', $settings);
            $log_id = $this->db->affected_rows();
            if ($log_id) {
                return 'Settings updated';
            } else {
                return 'Failed to update settings';
            }
        } else {
            $this->db->insert('settings', $settings);
            $resid = $this->db->insert_id();
            if ($resid) {
                return 'Settings Added';
            } else {
                return 'Failed to add settings';
            }
        }
    }
    public function updateRestaurantStatus($sts)
    {
        $sta_res =  $this->db->select('is_online')->where('partners_id', $sts)->where('partner_type', 2)->get('partners_login')->row();
        if ($sta_res->is_online == 0) {
            $this->db->set('is_online', 1)->where('partners_id', $sts)->where('partner_type', 2)->update('partners_login');
            return $this->db->affected_rows();
        } else {
            $this->db->set('is_online', 0)->where('partners_id', $sts)->where('partner_type', 2)->update('partners_login');
            return $this->db->affected_rows();
        }
    }

    public function getResSts()
    {
        $id = $_SESSION['uid'];
        return    $this->db->select('status')->where('restaurant_id ', $id)->get('restaurants')->row();
    }
    public function addNewTimeSlot($Rid, $pcode, $pcode2)
    {
        $p['time_slots_name'] = $pcode;
        $p['time_slots_name_end'] = $pcode2;
        $p['restaurant_id'] = $Rid;
        $this->db->insert('time_slots', $p);

        $resid = $this->db->insert_id();
    }
    public function newpincode($Rid, $pcode)
    {
        $p['restaurant_pincode'] = $pcode;
        $p['restaurant_id'] = $Rid;
        $this->db->insert('restaurant_area', $p);
        $resid = $this->db->insert_id();
    }
    public function getAllpincodes()
    {
        $id = $_SESSION['uid'];
        return  $this->db->where('restaurant_id', $id)->where('is_deleted', 0)->order_by('restaurant_area_id', 'desc')->get('restaurant_area')->result();
    }
    public function getAlltimeSlots()
    {
        $id = $_SESSION['uid'];
        return  $this->db->where('restaurant_id', $id)->where('is_deleted', 0)->order_by('time_slots_id', 'desc')->get('time_slots')->result();
    }
    public function timeslotsupdate($_mianId, $pcode, $endTime)
    {
        $this->db->set('time_slots_name', $pcode)->set('time_slots_name_end', $endTime)->where('time_slots_id ', $_mianId)->update('time_slots');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function pincodesupdate($_mianId, $pcode)
    {
        $this->db->set('restaurant_pincode', $pcode)->where('restaurant_area_id', $_mianId)->update('restaurant_area');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }
    public function pincodedelete($pcode)
    {
        $this->db->set('is_deleted', 1)->where('restaurant_area_id', $pcode)->update('restaurant_area');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }

    public function deletetimeslot($pcode)
    {
        $this->db->set('is_deleted', 1)->where('time_slots_id ', $pcode)->update('time_slots');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }

    public function deletedistance($pcode)
    {
        $this->db->set('is_deleted', 1)->where('id', $pcode)->update('deliverycharge_distance');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return true;
        }
        return false;
    }

    public function deletefoodmaster($masterId)
    {
        $this->db->set('is_deleted', 1)->where('master_id', $masterId)->update('food_master');
        $log_id = $this->db->affected_rows();
        if ($log_id) {
            return 'Food category deleted';
        }
        return 'failed to  deleted';
    }

    public function addNewToken($uid, $tokenId)
    {
        $arr = [
            'restaurant_id' => $uid,
            'token_id' => $tokenId,
        ];
        $tkn = $this->db->select('token_id')->where('restaurant_id', $uid)->where('is_active', 1)->get('restaurant_token')->row();
        if ($tkn) {
            if ($tokenId ==  $tkn->token_id) {
            } else {
                $this->db->set('token_id', $tokenId)->where('restaurant_id', $uid)->where('is_active', 1)->update('restaurant_token');
            }
        } else {
            $this->db->insert('restaurant_token',  $arr);
            return  $this->db->insert_id();
        }
    }
}
