<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Order_model extends CI_Model
{

    public function getnumOfRecords()
    {
        $rid = $_SESSION['uid'];
        return  $this->db->where('restaurant_id', $rid)->where('order_master.order_status >', 0)->get('order_master')->num_rows();
        //    $orders  =  $this->db->where('restaurant_id', $rid)->get('order_master')->result();
    }

    public function getnumOfRecordsSrch($fdnme)
    {
        $rid = $_SESSION['uid'];
        return   $allorders = $this->db->select('order_master.*,fud_sts_master.fud_status,fud_sts_master.fud_sts_master_id as fudcurntsts,users.first_name,users.mobile')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = order_master.order_status', 'inner')->join('users', 'users.id = order_master.customer_id', 'inner')->where('order_master.restaurant_id', $rid)->where('order_master.order_status >', 0)->like('order_master.order_master_id', $fdnme)->or_like('order_master.created_date', $fdnme)->or_like('users.first_name', $fdnme)->or_like('users.mobile', $fdnme)->order_by('order_master.order_master_id', 'desc')->get('order_master')->result();
        //    $orders  =  $this->db->where('restaurant_id', $rid)->get('order_master')->result();
    }

    public function getAllOrders($limit, $offset)
    {
        $rid = $_SESSION['uid'];
        $allorders = $this->db->select('order_master.*,fud_sts_master.fud_status,fud_sts_master.fud_sts_master_id as fudcurntsts')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = order_master.order_status', 'inner')->where('order_master.restaurant_id', $rid)->where('order_master.order_status >', 0)->limit($limit)->offset($offset)->order_by('order_master.order_master_id', 'desc')->get('order_master')->result();
        $orders = [];
        foreach ($allorders as $key => $ord_val) {
            $orderOBJ = new stdClass();
            $orderOBJ->orderId = $ord_val->order_master_id;
            $orderOBJ->createdDate = $ord_val->created_date;
            $orderOBJ->order_amount = $ord_val->order_amount;
            $orderOBJ->order_status = $ord_val->fud_status;
            $orderOBJ->tax = $ord_val->tax_amount;
            $orderOBJ->order_status_val = $ord_val->fudcurntsts;
            $orderOBJ->foodItems = $this->getOrderItems($ord_val->cart_master_id);
            $orderOBJ->countOfItems = $this->countOfItems($ord_val->cart_master_id);
            $orders[] = $orderOBJ;
        }
        return  $orders;
    }

    function getOrderItems($id)
    {
        return   $this->db->select('cart_details.selling_price,cart_details.food_name,cart_details.quantity,cart_details.food_unit,food_units.master_id as foodId,food_master.item_name')->join('food_units', 'food_units.unit_id = cart_details.food_unit_id', 'inner')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('cart_details.cart_master_id', $id)->get('cart_details')->result();
    }

    function countOfItems($id)
    {
        return   $this->db->select('cart_details.food_name,cart_details.quantity,cart_details.food_unit,food_units.master_id as foodId,food_master.item_name')->join('food_units', 'food_units.unit_id = cart_details.food_unit_id', 'inner')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('cart_details.cart_master_id', $id)->get('cart_details')->num_rows();
    }

    public function updateorderstatus($orderid, $status)
    {
        $this->db->set('order_status', $status)->where('order_master_id', $orderid)->update('order_master');
        $this->db->affected_rows();
        $track = array(
            'order_id' => $orderid,
            'restaurant_id' => $_SESSION['uid'],
            'status' => $status,
        );
        $this->db->insert('track_order', $track);
        return $this->db->insert_id();
    }
    public function cancelorder($orderid, $status, $reason)
    {
        $this->db->set('order_status', $status)->set('cancel_reason', $reason)->where('order_master_id', $orderid)->update('order_master');
        $this->db->affected_rows();
        $track = array(
            'order_id' => $orderid,
            'restaurant_id' => $_SESSION['uid'],
            'status' => $status,
        );
        $this->db->insert('track_order', $track);
        return $this->db->insert_id();
    }

    public function getOrderById($id)
    {
        $orders = [];
        $orders['orderdetails'] =  $this->db->select('order_master.*,users_address.landmark,users_address.location_address,users_address.address,users_address.pincode,users.first_name,users.mobile,')->join('users_address', 'users_address.users_address_id = order_master.address_id')->join('users', 'users.id = order_master.customer_id')->where('users_address.is_default', 1)->where('order_master.order_master_id', $id)->get('order_master')->row();

        $cartid = $this->db->select('cart_master_id')->where('order_master_id', $id)->get('order_master')->row();

        $orders['orderItems'] =  $this->db->select('cart_details.selling_price,cart_details.food_name,cart_details.quantity,cart_details.food_unit,cart_details.message,food_units.master_id as foodId,food_master.item_name,food_master.total_tax')->join('food_units', 'food_units.unit_id = cart_details.food_unit_id', 'inner')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('cart_details.cart_master_id', $cartid->cart_master_id)->get('cart_details')->result();

        $orders['del_address'] =  $this->db->select('order_master.customer_id,users_address.*')->join('users_address', 'users_address.user_id = order_master.customer_id', 'inner')->where('order_master.order_master_id', $id)->where('users_address.is_default', 1)->get('order_master')->row();

        return $orders;
    }
    public function getBankDetails($id, $deliveryMode, $time_slot)
    {
        $main = [];
        $mde = $this->db->select('delivery_mode_type as mode_type')->where('delivery_mode_id', $deliveryMode)->get('delivery_mode')->row();
        $main['del_mode'] = $mde->mode_type;

        $tmeslt = $this->db->select('time_slots_name  as starttime, time_slots_name_end as endtime')->where('time_slots_id', $time_slot)->get('time_slots')->row();
        $tmeslt->mode_type;
        $main['starttime'] =  date('h:i A', strtotime($tmeslt->starttime));
        $main['endtime'] =  date('h:i A', strtotime($tmeslt->endtime));
        return  $main;
    }
    public function getOrderStatus()
    {
        return  $this->db->where('is_active', 1)->order_by('fud_sts_master_id', 'asc')->get('fud_sts_master')->result();
    }

    public function getMyOrders($limit, $offset)
    {
        $rid = $_SESSION['uid'];
        $allorders = $this->db->select('order_master.*,fud_sts_master.fud_status,fud_sts_master.fud_sts_master_id as fudcurntsts,users.first_name,users.mobile')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = order_master.order_status', 'inner')->join('users', 'users.id = order_master.customer_id', 'inner')->where('order_master.restaurant_id', $rid)->where('order_master.order_status >', 0)->limit($limit)->offset($offset)->order_by('order_master.order_master_id', 'desc')->get('order_master')->result();
        $orders = [];
        foreach ($allorders as $key => $ord_val) {
            $orderOBJ = new stdClass();
            $orderOBJ->orderId = $ord_val->order_master_id;
            $orderOBJ->createdDate = $ord_val->created_date;
            $orderOBJ->order_amount = $ord_val->order_amount;
            $orderOBJ->order_status = $ord_val->fud_status;
            $orderOBJ->order_status_val = $ord_val->fudcurntsts;
            $orderOBJ->tax = $ord_val->tax_amount;
            $orderOBJ->customer = $this->getCustomerDetails($ord_val->customer_id);

            $orders[] = $orderOBJ;
        }
        return  $orders;
    }

    public function getMyOrdersSearch($fdnme)
    {
        $rid = $_SESSION['uid'];
        $allorders = $this->db->select('order_master.*,fud_sts_master.fud_status,fud_sts_master.fud_sts_master_id as fudcurntsts,users.first_name,users.mobile')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = order_master.order_status', 'inner')->join('users', 'users.id = order_master.customer_id', 'inner')->where('order_master.restaurant_id', $rid)->where('order_master.order_status >', 0)->like('order_master.order_master_id', $fdnme)->or_like('order_master.created_date', $fdnme)->or_like('users.first_name', $fdnme)->or_like('users.mobile', $fdnme)->order_by('order_master.order_master_id', 'desc')->get('order_master')->result();
        $orders = [];
        foreach ($allorders as $key => $ord_val) {
            $orderOBJ = new stdClass();
            $orderOBJ->orderId = $ord_val->order_master_id;
            $orderOBJ->createdDate = $ord_val->created_date;
            $orderOBJ->order_amount = $ord_val->order_amount;
            $orderOBJ->order_status = $ord_val->fud_status;
            $orderOBJ->order_status_val = $ord_val->fudcurntsts;
            $orderOBJ->tax = $ord_val->tax_amount;
            $orderOBJ->customer = $this->getCustomerDetails($ord_val->customer_id);

            $orders[] = $orderOBJ;
        }
        return  $orders;
    }

    public function getCustomerDetails($id)
    {
        return  $this->db->select('first_name,mobile')->where('id', $id)->get('users')->row();
    }
}
