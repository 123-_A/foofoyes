<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Restaurant_model extends CI_Model
{
    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */
    public function getSelectedRestaurant($restaurantId, $restaurantType, $latitude, $longitude)
    {
        $restauranthome = [];
        $allrestaurant =   $this->db->query("SELECT `restaurants`.`restaurant_id`,`restaurants`.`latitude`,`restaurants`.`longitude`, `settings`.`maximum_delivery_distance`,`partners_login`.`is_active`
        FROM `restaurants`
        INNER JOIN `settings` ON `settings`.`restaurant_id` = `restaurants`.`restaurant_id`
        INNER JOIN `partners_login` ON `partners_login`.`partners_id` = `restaurants`.`restaurant_id`
        WHERE `restaurants`.`restaurant_id` = $restaurantId AND `partners_login`.`is_active` = 1")->row();


        if (!empty($allrestaurant)) {


            $res_lati = $allrestaurant->latitude;
            $res_longi = $allrestaurant->longitude;
            // $respicode = $allres_val->pincode;
            // $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$postalcode&destinations=$respicode&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);

            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];


            $restResult =   $this->db->query("SELECT `restaurants`.`restaurant_id`,`restaurants`.`district`,`restaurants`.`city`,`restaurants`.`reasturant_image_one`,`restaurants`.`reasturant_image_two`,`restaurants`.`reasturant_image_three`,`restaurant_name`, `restaurants`.`contact_no`, `restaurants`.`pincode`, `restaurants`.`longitude`, `restaurants`.`latitude`, `restaurants`.`status`,`restaurants`.`restaurant_category`,`partners_login`.`is_active`,`district`.`district_name`,`city`.`city_name` FROM `restaurants` 
            INNER JOIN `partners_login` ON `partners_login`.`partners_id` = `restaurants`.`restaurant_id`
            INNER JOIN `district` ON `district`.`district_id` = `restaurants`.`district`
            INNER JOIN `city` ON `city`.`city_id` = `restaurants`.`city`
             WHERE `restaurants`.`restaurant_id` = $allrestaurant->restaurant_id AND `restaurants`.`status` = 1 AND `partners_login`.`is_online` = 1 AND `partners_login`.`partner_type` = 2")->row();
            if ($restResult) {
                $nearRestaurant = new stdClass();

                $nearRestaurant->restaurant_id = $restResult->restaurant_id;
                $nearRestaurant->restaurant_name = $restResult->restaurant_name;
                $nearRestaurant->pincode = $restResult->pincode;
                $nearRestaurant->distance = $dist;
                $nearRestaurant->longitude = $restResult->longitude;
                $nearRestaurant->latitude = $restResult->latitude;
                $nearRestaurant->district = $restResult->district_name;
                $nearRestaurant->city = $restResult->city_name;
                $nearRestaurant->restaurant_category = $restResult->restaurant_category;
                $nearRestaurant->reasturant_image_one = base_url() . $restResult->reasturant_image_one;
                $nearRestaurant->reasturant_image_two = base_url() . $restResult->reasturant_image_two;
                $nearRestaurant->reasturant_image_three = base_url() . $restResult->reasturant_image_three;
                return $nearRestaurant;
            } else {
                return false;
            }
        }
    }

    public function foodswithoffers($restaurantId)
    {
        $offers = [];
        // select  category with  listing by   time
        $curenttime = date('H:i');



        $wre = " '$curenttime' between starting_time AND ending_time AND is_active = 1 ";
        $category =  $this->db->select('group_id')->where('restaurant_id', $restaurantId)->where($wre)->get('fud_timing')->result();
        $allgroup = [];
        foreach ($category  as $k => $catvl) {
            $allgroup[] = $catvl->group_id;
        }
        $avilGrps = (implode(",", $allgroup));



        // list offer fud by category
        // $results =  $this->db->select('food_master.master_id as foodId,food_master.item_name,food_master.description,food_master.food_cat_id,food_master.food_type,food_master.food_group_id,food_master.food_image,food_master.thump_image,food_master.total_tax,food_units.unit_id as variantId,food_units.variant_name,food_units.display_price,food_units.offer_price,food_units.max_prod_quantity')
        //     // ->join('food_category', 'food_category.food_category_id =  food_master.food_cat_id', 'inner')
        //     // ->join('food_groups', 'food_groups.foodgroupId = food_master.food_group_id', 'inner')
        //     ->join('food_units', 'food_units.master_id = food_master.master_id', 'inner')
        //     ->where_in('food_master.food_group_id', $avilGrps)
        //     ->where('food_units.status', 1)
        //     ->where('food_units.activate_offer', 1)
        //     ->where('food_master.restaurant_status', 1)
        //     ->where('food_master.is_active', 1)
        //     ->where('food_master.is_deleted', 0)
        //     ->where('food_master.restaurant_id', $restaurantId)
        //     ->get('food_master')->result();
        // echo $this->db->last_query();
        // die();

        if ($avilGrps) :
            $results =   $this->db->query("SELECT `food_master`.`master_id` as `foodId`, `food_master`.`item_name`, `food_master`.`description`,
        `food_master`.`food_cat_id`, `food_master`.`food_type`, `food_master`.`food_group_id`, `food_master`.`food_image`,
        `food_master`.`thump_image`, `food_master`.`total_tax`, `food_units`.`unit_id` as `variantId`,
        `food_units`.`variant_name`, `food_units`.`display_price`, `food_units`.`offer_price`, `food_units`.max_prod_quantity
        FROM `food_master`
        INNER JOIN `food_units` ON `food_units`.`master_id` = `food_master`.`master_id`
        WHERE `food_master`.`food_group_id` IN($avilGrps)
        AND `food_units`.`status` = 1
        AND `food_units`.`activate_offer` = 1
        AND `food_master`.`restaurant_status` = 1
        AND `food_master`.`is_active` = 1
        AND `food_master`.`is_deleted` = 0
        AND `food_master`.`restaurant_id` = '$restaurantId'")->result();

            if ($results) {
                foreach ($results as $keys => $rsltvl) {
                    $resobj = new stdClass();
                    $resobj->foodId = $rsltvl->foodId;
                    $resobj->catId = $rsltvl->food_cat_id;
                    $resobj->catName = $this->catName($rsltvl->food_cat_id);
                    $resobj->item_name = $rsltvl->item_name;
                    $resobj->description = $rsltvl->description;
                    $resobj->food_image = base_url() . $rsltvl->food_image;
                    $resobj->thump_image = base_url() . $rsltvl->thump_image;
                    $resobj->variantId = $rsltvl->variantId;
                    $resobj->variant_name = $rsltvl->variant_name;
                    if ($rsltvl->custom_message) {
                        $resobj->custom_message = true;
                    } else {
                        $resobj->custom_message = false;
                    }
                    $resobj->max_quantity = (int)$rsltvl->max_prod_quantity * 100;
                    $resobj->display_price = (int)$rsltvl->display_price * 100;
                    $resobj->offer_price = (int)$rsltvl->offer_price * 100;
                    $resobj->dicsountpercen = $this->discount($rsltvl->offer_price, $rsltvl->display_price);
                    $offers[] = $resobj;
                }
            }
            return ($offers);
        endif;
    }


    public function discount($ofrprc, $dispPrice)
    {
        if ($ofrprc <= 0) {
            return strval(0);
        } else {
            $perc = (($dispPrice - $ofrprc) * 100) / $dispPrice;
            $val = number_format($perc, 2);
            return (int)$val;
        }
    }

    public function listCategory($restaurantId, $restaurantType, $offset)
    {
        $allcats = [];
        if ($restaurantType == 'veg') {
            $fudtype = 'veg';
        } else {
            $fudtype = 'non-veg';
        }

        $limit = 5;
        $this->db->select('food_master.food_cat_id,food_category.food_category_name,food_category.food_category_icon')->join('food_category', 'food_category.food_category_id = food_master.food_cat_id', 'inner')->where('food_master.food_type', $fudtype)->where('food_master.restaurant_status', 1)->where('food_master.restaurant_id', $restaurantId)->where('food_category.is_active', 1)->group_by('food_master.food_cat_id')->order_by('food_category.food_category_id', 'asc');
        // if ($offset > 0) {
        //     $cnt = $offset * 5;
        //     $this->db->limit($limit, $cnt);
        // } else {
        //     $this->db->limit($limit);
        // }

        $catgs = $this->db->get('food_master')->result();

        if ($offset <= 0) {
            $a = (object) array('food_cat_id' => 0, 'food_category_name' => 'All', 'food_category_icon' => null);
            array_unshift($catgs, $a);
        }
        foreach ($catgs as $key =>  $catgs_vl) {

            $catobj = new stdClass();
            $catobj->categoryId = $catgs_vl->food_cat_id;
            $catobj->categoryName = $catgs_vl->food_category_name;
            $catobj->categoryIcon = base_url() . $catgs_vl->food_category_icon;
            $catobj->categoryId = $catgs_vl->food_cat_id;
            $catobj->categoryId = $catgs_vl->food_cat_id;
            $allcats[] = $catobj;
            // print_r($allcats);
        }
        if ($allcats) {
            return $allcats;
        } else {
            return [];
        }
    }

    public function listFudsById($restaurantId, $restaurantType, $offset, $categoryId)
    {
        $allfuds = [];
        $resResult = '';
        $limit = 10;
        if ($offset > 0) {
            $cnt = $offset * 10;
            $lm = $cnt . ',' . $limit;
        } else {
            $lm = $limit;
        }


        if ($restaurantType == 'veg') {
            $fudtype = 'veg';
        } else {
            $fudtype = 'non-veg';
        }
        // select  category with  listing by   time
        $curenttime = date('H:i');

        $wre = " '$curenttime' between starting_time AND ending_time AND is_active = 1 ";
        $category =  $this->db->select('group_id')->where('restaurant_id', $restaurantId)->where($wre)->get('fud_timing')->result();

        $allgroup = [];
        foreach ($category  as $k => $catvl) {
            $allgroup[] = $catvl->group_id;
        }
        $avilGrps = (implode(",", $allgroup));

        // list offer fud by category Id
        if ($categoryId == 0) {
            if ($category) {
                // $resResult = $this->db->select('food_master.*')->where_in('food_master.food_group_id', $avilGrps)->where('food_master.restaurant_status', 1)
                //     ->where('food_master.restaurant_id', $restaurantId)
                //     // ->where('food_master.food_cat_id', $categoryId)
                //     ->where('food_master.food_type', $fudtype)
                //     ->where('food_master.is_active', 1)
                //     ->where('food_master.is_deleted', 0)->get('food_master')->result();

                $resResult =     $this->db->query("SELECT `food_master`.* FROM `food_master` WHERE `food_master`.`food_group_id` IN($avilGrps) AND `food_master`.`restaurant_status` = 1 AND `food_master`.`restaurant_id` = '$restaurantId' AND `food_master`.`food_type` = '$fudtype' AND `food_master`.`is_active` = 1 AND `food_master`.`is_deleted` = 0 LIMIT $lm ")->result();
                // echo $this->db->last_query();
            }
        } else {
            if ($category) {


                $resResult =     $this->db->query("SELECT `food_master`.* FROM `food_master` WHERE `food_master`.`food_group_id` IN($avilGrps) AND `food_master`.`restaurant_status` = 1 AND `food_master`.`restaurant_id` = '$restaurantId' AND `food_master`.`food_cat_id` = '$categoryId'
                     AND `food_master`.`food_type` = '$fudtype' AND `food_master`.`is_active` = 1 AND `food_master`.`is_deleted` = 0 LIMIT $lm ")->result();
            }
        }    // 
        if ($resResult) :
            foreach ($resResult as $keys => $rsVal) {
                $fuds = new stdClass();
                $fuds->foodId = $rsVal->master_id;
                $fuds->catName = $this->catName($rsVal->food_cat_id);
                $fuds->restaurant_id = $rsVal->restaurant_id;
                $fuds->food_name = $rsVal->item_name;
                $fuds->description = $rsVal->description;
                $fuds->food_cat_id = $rsVal->food_cat_id;
                $fuds->food_type = $rsVal->food_type;
                $fuds->food_image = base_url() . $rsVal->food_image;
                $fuds->thump_image = base_url() . $rsVal->thump_image;
                if ($rsVal->custom_message) {
                    $fuds->custom_message = true;
                } else {
                    $fuds->custom_message = false;
                }
                $has_offer = $this->checkOffers($rsVal->master_id);
                $fuds->has_offer = $this->checkOffers($rsVal->master_id);
                if ($has_offer == 'true') {
                    $fuds->offer = $this->getOffers($rsVal->master_id);
                }
                $fuds->foodvariants = $this->getAllFudVariants($rsVal->master_id);
                $allfuds[] = $fuds;
            }
            return  $allfuds;
        endif;
    }

    function catName($id)
    {
        $is_offer =  $this->db->select('food_category_name')->where('food_category_id', $id)->where('is_active', 1)->get('food_category')->row();
        return $is_offer->food_category_name;
    }
    public function checkOffers($id)
    {
        $has = '';
        $is_offer =  $this->db->select('activate_offer')->where('master_id', $id)->where('is_deleted', 0)->get('food_units')->result();

        foreach ($is_offer as $has_offer) :

            if ($has_offer->activate_offer == 1) {

                $has = 'true';
                break;
            } else {
                $has = 'false';
                continue;
            }
        endforeach;

        return $has;
    }

    public function getOffers($id)
    {
        $get_offer =  $this->db->select('sum(offer_price) as offer_price_total,sum(display_price) as display_price_total,display_price,offer_price')->order_by('unit_id', 'asc')->where('master_id', $id)->where('is_deleted', 0)->where('is_active', 1)->where('status', 1)->get('food_units')->row();

        $ofrs = new stdClass();
        $ofrs->offer_price = (int)$get_offer->offer_price * 100;
        $ofrs->display_price = (int) $get_offer->display_price * 100;
        $ofrs->offer_price_total = $this->getTotalOfPfferPrice($id);
        $ofrs->display_price_total = $this->getTotalOfDispPrice($id);

        $ofrs->dicsountpercen = $this->discount($ofrs->offer_price_total, $ofrs->display_price_total);
        return $ofrs;
    }

    public function getTotalOfPfferPrice($id)
    {
        $get_offer =  $this->db->select('sum(offer_price) as offer_price_total,sum(display_price) as display_price_total,display_price,offer_price')->order_by('unit_id', 'asc')->where('activate_offer', 1)->where('master_id', $id)->where('is_deleted', 0)->where('is_active', 1)->where('status', 1)->get('food_units')->row();
        return   (int)$get_offer->offer_price_total * 100;
    }
    public function getTotalOfDispPrice($id)
    {
        $get_offer =  $this->db->select('sum(offer_price) as offer_price_total,sum(display_price) as display_price_total,display_price,offer_price')->order_by('unit_id', 'asc')->where('activate_offer', 1)->where('master_id', $id)->where('is_deleted', 0)->where('is_active', 1)->where('status', 1)->get('food_units')->row();
        return   (int)$get_offer->display_price_total * 100;
    }

    public function getAllFudVariants($masterId)
    {
        $allunits = $this->db->select('unit_id as food_variantId,variant_name as foodname,unit,offer_price,display_price,max_prod_quantity,activate_offer')->where('master_id', $masterId)->where('is_deleted', 0)->where('status', 1)->where('is_active', 1)->get('food_units')->result();

        $units = [];
        foreach ($allunits as $uvals) {
            $newunits = new stdClass();
            $newunits->food_variantId = $uvals->food_variantId;
            $newunits->foodname = $uvals->foodname;
            $newunits->unit = $uvals->unit;
            $newunits->offer_price = (int)$uvals->offer_price * 100;
            $newunits->display_price = (int) $uvals->display_price * 100;
            $newunits->max_prod_quantity = (int)$uvals->max_prod_quantity;
            if ($uvals->activate_offer == 1) {
                $newunits->offer = true;
            } else {
                $newunits->offer = false;
            }
            $units[] = $newunits;
        }
        return $units;
    }
}
