<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Hashtag_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     * 
     * 
     */

    public function addNewHasTag($tag)
    {
        $this->db->insert('hash_tag', $tag);
        $resid = $this->db->insert_id();
        if ($resid) {
            return true;
        } else {
            return false;
        }
    }
    public function getAllHash()
    {
        return   $this->db->get('hash_tag')->result();
    }
    public function deleteNewHasTag($delId)
    {
        $sta_res =  $this->db->where('hash_tag_id', $delId)->delete('hash_tag');

        return $this->db->affected_rows();
    }
}
