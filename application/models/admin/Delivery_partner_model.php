<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Delivery_partner_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function newFoodcategory($catarr)
    {
        $this->db->insert('food_category', $catarr);
        return $this->db->insert_id();
    }
    public function newFoodVariant($arr)
    {
        $this->db->insert('food_variant', $arr);
        return $this->db->insert_id();
    }
    public function newBannerData($bnr)
    {
        $this->db->insert('ad_promotion', $bnr);
        return $this->db->insert_id();
    }
    public function addNewDelPartner($partner, $uname, $pass, $latti, $longi)
    {
        $this->db->insert('delivery_partner', $partner);
        $insid = $this->db->insert_id(); {
            $login['partners_id'] =  $insid;
            $login['username'] = $uname;
            $login['password'] =  md5($pass);
            $login['partner_type'] =  3;
            $login['longitude'] =  $longi;
            $login['latitude'] =  $latti;
            $this->db->insert('partners_login', $login);
            $log_id = $this->db->insert_id();
            if ($log_id) {
                return true;
            } else {
                $this->db->where('restaurant_id', $insid)->delete('delivery_partner');
                return false;
            }
        }
    }
    public function getAlldeliveryPartners()
    {
        return   $this->db->select('delivery_partner.*,district.district_name,city.city_name')->join('district', 'district.district_id = delivery_partner.destrict_id')->join('city', 'city.city_id = delivery_partner.city_id')->get('delivery_partner')->result();
    }
}
