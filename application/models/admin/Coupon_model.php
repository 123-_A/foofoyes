<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Coupon_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */



    public function addNewPoints($_froms, $_tos, $_pnts, $id)
    {
        $arr = array(
            'view_from' => $_froms,
            'view_to' => $_tos,
            'points' => $_pnts,
        );
        if (!$id) {
            $this->db->insert('coupon_values', $arr);
            return $this->db->insert_id();
        } else {
            $this->db->where('coupon_values_id', $id)->update('coupon_values', $arr);
            return $this->db->affected_rows();
        }
    }

    public function getPoints()
    {
        return  $this->db->select('coupon_values.*')->where('is_deleted', 0)->order_by('coupon_values_id', 'desc')->get('coupon_values')->result();
    }

    public function geteditdata($id)
    {
        return  $this->db->select('coupon_values.*')->where('coupon_values_id', $id)->where('is_deleted', 0)->order_by('coupon_values_id', 'desc')->get('coupon_values')->row();
    }
    public function deletepoints($id)
    {
        $this->db->set('is_deleted', 1)->where('coupon_values_id', $id)->update('coupon_values');
        return $this->db->affected_rows();
    }
    public function getSettings()
    {
        return  $this->db->select('coupon_settings.*')->where('is_deleted', 0)->get('coupon_settings')->row();
    }
    public function updatepointvalue($_vId)
    {
        $this->db->set('one_point_value', $_vId)->update('coupon_settings');
        return $this->db->affected_rows();
    }

    public function updateminimumpnt($_vId)
    {
        $this->db->set('minimum_points_required', $_vId)->update('coupon_settings');
        return $this->db->affected_rows();
    }
    public function updateorderamount($_vId)
    {
        $this->db->set('minimun_amount_required', $_vId)->update('coupon_settings');
        return $this->db->affected_rows();
    }
}
