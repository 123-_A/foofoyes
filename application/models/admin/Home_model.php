<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function addNewresturant($resturant, $uname, $password, $lon, $lat)
    {
        $this->db->insert('restaurants', $resturant);
        $resid = $this->db->insert_id();
        if ($resid) {
            $login['partners_id'] =  $resid;
            $login['username'] = $uname;
            $login['password'] =  md5($password);
            $login['partner_type'] =  2;
            $login['longitude'] =  $lon;
            $login['latitude'] =  $lat;
            $this->db->insert('partners_login', $login);
            $log_id = $this->db->insert_id();
            if ($log_id) {
                return true;
            } else {
                $this->db->where('restaurant_id', $resid)->delete('restaurants');
                return false;
            }
        } else {
            return false;
        }
    }
}
