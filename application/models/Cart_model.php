<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cart_model extends CI_Model
{



    public function addCartMaster($id)
    {
        $FDmasterId =  $this->db->select('food_units.master_id as in_unit_master,food_master.master_id')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('food_units.unit_id', $id)->get('food_units')->row();

        // print_r($FDmasterId);

        $foodmasterid = [];

        // foreach()
    }

    public function getSumOfFudTaxes($fudId)
    {
        $alltax = [];
        $amt = 0;
        $i = 0;
        foreach ($fudId as $kwy => $fudVal) {
            $i++;
            if (count($fudId) == $i) {
                $pizza =   $this->db->select('total_tax')->where('master_id', $fudVal->foodId)->get('food_master')->row();
                $pieces = explode(" ", $pizza->total_tax);

                $amt +=  trim($pieces[1]);

                $alltax[] =  $amt;
            }
        }
        if (count($alltax) > 0)
            return $alltax;
    }

    public function getTotalAmount($id, $quantity)
    {
        $totalamount = [];
        $total = 0;
        $amount =  $this->db->select('food_master.total_tax,food_units.display_price,food_units.offer_price,food_units.activate_offer')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('food_units.unit_id', $id)->get('food_units')->row();
        $pieces = explode(" ", $amount->total_tax);
        if ($amount->activate_offer == 1) {
            $tax = ($amount->offer_price *  $quantity) * $pieces[1] / 100;
            return   $total += ($amount->offer_price *  $quantity) + $tax;
        } else {
            $tax = ($amount->display_price *  $quantity) * $pieces[1] / 100;
            return  $total += ($amount->display_price *  $quantity) + $tax;
        }
    }

    public function getTotalAmountForTax($id, $quantity)
    {
        $totalamount = [];
        $total = 0;
        $amount =  $this->db->select('food_master.total_tax,food_units.display_price,food_units.offer_price,food_units.activate_offer')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('food_units.unit_id', $id)->get('food_units')->row();
        $pieces = explode(" ", $amount->total_tax);
        if ($amount->activate_offer == 1) {

            return   $total += ($amount->offer_price *  $quantity);
        } else {

            return  $total += ($amount->display_price *  $quantity);
        }
    }


    public function addNewcartMaster($restaurantId, $userId, $payableAmount, $gstAmount, $totlAmt)
    {
        $cart = array(
            'restaurant_id' => $restaurantId,
            'customer_id' => $userId,
            'amount' => $totlAmt,
            'total_amount' => $payableAmount,
            'created_date' => date('Y-m-d H:i:s'),
            'is_order_placed' => 0,
            'cart_master_status' => 'Added to cart',
            'tax_amount' => $gstAmount
        );
        $this->db->insert('cart_master', $cart);
        return  $this->db->insert_id();
    }

    public function addCartItems($cart, $masterId)
    {
        $i = 0;
        foreach ($cart as $keys => $cval) {
            $i++;
            $foodData =  $this->getNeededCartDatas($cval->variantId);
            $price = $this->getFoodPrice($cval->variantId, $cval->quantity);
            $cartDetails = array(
                'cart_master_id' => $masterId,
                'food_name' =>  $foodData->variant_name,
                'food_unit' => $foodData->unit,
                'food_unit_id' => $foodData->unit_id,
                'quantity' => $cval->quantity,
                'selling_price' =>  $price,
                'message' => $cval->message,
            );
            $this->db->insert('cart_details', $cartDetails);
            if (count($cart) == $i) {
                return true;
            }
        }
    }



    function getNeededCartDatas($id)
    {
        return   $this->db->select('master_id,variant_name,unit,unit_id')->where('unit_id', $id)->get('food_units')->row();
    }
    function getFoodPrice($id, $qty)
    {
        $amount =  $this->db->select('display_price,offer_price,activate_offer')->where('unit_id', $id)->get('food_units')->row();
        if ($amount->activate_offer == 1) {
            return    $amount->offer_price * $qty;
        } else {
            return  $amount->display_price * $qty;
        }
    }

    public function getCodCheckOutPage($userId, $cartId, $latitude, $longitude, $restaurantId)
    {
        // get distance 
        $res_val =  $this->db->select('latitude,longitude')->where('restaurant_id', $restaurantId)->get('restaurants')->row();

        $res_lati = trim($res_val->latitude);

        $res_longi = trim($res_val->longitude);


        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

        $imp = explode(' ', $dist);


        if ($imp[1] == 'km') {
            $distances = floor($imp[0]);
        } else {
            $distances  = 1;
        }

        $wre = " $distances between minimum_distance and maximum_distance";
        $deliverycharge =  $this->db->select('delivery_charge')->where($wre)->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->get('deliverycharge_distance')->row();
        // print_r($deliverycharge);
        if ($deliverycharge) {
            $cmaster = $this->db->select('delivery_charge')->where('cart_master_id', $cartId)->get('cart_master')->row();
            if ($cmaster->delivery_charge <= 0) {
                $this->db->query("UPDATE `cart_master` SET  total_amount = total_amount + $deliverycharge->delivery_charge,`delivery_charge` = $deliverycharge->delivery_charge WHERE `cart_master_id` = $cartId");
            }
            $rslt =   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId,delivery_charge,amount,delivery_charge')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();

            return (object)array('grandtotal' => $rslt->payable, 'itemTotal' => $rslt->amount, 'tax' => $rslt->tax, 'cartId' => $rslt->cartId, 'distance' => $dist, 'delivery' => $rslt->delivery_charge);
        } else {

            return 'delivery distance not added';
        }
        // return   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();
    }
    public function getBankCheckOutPage($userId, $cartId, $latitude, $longitude, $restaurantId)
    {

        // get distance 
        $res_val =  $this->db->select('latitude,longitude')->where('restaurant_id', $restaurantId)->get('restaurants')->row();

        $res_lati = trim($res_val->latitude);

        $res_longi = trim($res_val->longitude);


        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

        $imp = explode(' ', $dist);


        if ($imp[1] == 'km') {
            $distances = floor($imp[0]);
        } else {
            $distances  = 1;
        }

        $wre = " $distances between minimum_distance and maximum_distance";
        $deliverycharge =  $this->db->select('delivery_charge')->where($wre)->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->get('deliverycharge_distance')->row();

        if ($deliverycharge) {

            $cmaster = $this->db->select('delivery_charge')->where('cart_master_id', $cartId)->get('cart_master')->row();
            if ($cmaster->delivery_charge <= 0) {
                $this->db->query("UPDATE `cart_master` SET  total_amount = total_amount + $deliverycharge->delivery_charge,`delivery_charge` = $deliverycharge->delivery_charge WHERE `cart_master_id` = $cartId");
            }
            $rslt =   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId,delivery_charge,amount,delivery_charge')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();

            return (object)array('grandtotal' => $rslt->payable, 'itemTotal' => $rslt->amount, 'tax' => $rslt->tax, 'cartId' => $rslt->cartId, 'distance' => $dist, 'delivery' => $rslt->delivery_charge);

            // $this->db->set('delivery_charge', $deliverycharge->delivery_charge)->where('cart_master_id', $cartId)->update('cart_master');

            // return   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId,delivery_charge')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();
        }
    }

    public function getDeliveryMode($restaurantId)
    {
        $timeslots = [];
        $result =  $this->db->select('delivery_mode_id,delivery_mode_type')->where('is_deleted', 0)->get('delivery_mode')->result();
        if ($result) {
            foreach ($result as $rval) {
                $delMode = new stdClass();
                $delMode->id = $rval->delivery_mode_id;
                $delMode->type = $rval->delivery_mode_type;
                $delMode->timeslot = $this->getAvailTimeSlot($restaurantId, $rval->delivery_mode_id);
                $timeslots[] = $delMode;
            }
            return  $timeslots;
        }
    }

    public function getAvailTimeSlot($restaurantId, $id)
    {

        if ($id == 1) {
            return [];
        } else if ($id == 2) {
            $time = date('H:i');
            $result =  $this->db->select('time_slots_name as startTime,time_slots_name_end as endTime,time_slots_id as timeId')->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->where('time_slots_name >', $time)->get('time_slots')->result();
            $pre = [];
            foreach ($result as $k => $vl) {
                $timeObj = new stdClass();
                $timeObj->startTime = date('h:i A', strtotime($vl->startTime));
                $timeObj->endTime = date('h:i A', strtotime($vl->endTime));
                $timeObj->timeId = $vl->timeId;
                $pre[] = $timeObj;
            }
            return $pre;
        } else {
            $result =  $this->db->select('time_slots_name as startTime ,time_slots_name_end as endTime,time_slots_id as timeId')->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->get('time_slots')->result();
            $pre = [];
            foreach ($result as $k => $vl) {
                $timeObj = new stdClass();
                $timeObj->startTime = date('h:i A', strtotime($vl->startTime));
                $timeObj->endTime = date('h:i A', strtotime($vl->endTime));
                $timeObj->timeId = $vl->timeId;
                $pre[] = $timeObj;
            }
            return $pre;
        }
    }

    public function updateCodCheckOut($cartId, $userId, $token, $longitude, $latitude)
    {
        $this->db->set('is_order_placed', 1)->set('cart_master_status', 1)->set('paymentmethod', 'COD')->where('cart_master_id', $cartId)->update('cart_master');
        $updateSts = $this->db->affected_rows();
        if ($updateSts > 0) {
            $cartDetails = $this->db->where('cart_master_id', $cartId)->get('cart_master')->row();

            $deliveryAddress = $this->db->where('user_id', $userId)->where('is_default', 1)->where('is_deleted', 0)->get('users_address')->row();
            $this->updateCouponPoints($userId, $cartDetails->coupon);
            $orderMaster = array(
                'cart_master_id' => $cartId,
                'deliveryMode' => 0,
                'restaurant_id' => $cartDetails->restaurant_id,
                'customer_id' => $userId,
                'order_amount' => $cartDetails->total_amount,
                'refund_amount' => 0,
                'order_status' => $cartDetails->cart_master_status,
                'payment_status' => 1,
                'payment_transaction_id' => 0,
                'razorpay_orderId' => 0,
                'delivery_address' => $deliveryAddress->address,
                'address_id' => $deliveryAddress->users_address_id,
                'delivery_time_from ' => 0,
                'delivery_time_to' => 0,
                'time_slot' => 0,
                'delivery_date' => date('Y-m-d H:i:s'),
                'delivery_charge' => $cartDetails->delivery_charge,
                'tax_amount' => $cartDetails->tax_amount,
                'coupon_code' => isset($cartDetails->coupon) ? $cartDetails->coupon : 0,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'bag_num' => 0,
                'payment_method' => isset($cartDetails->paymentmethod) ? $cartDetails->paymentmethod : 0,
            );

            $this->db->insert('order_master', $orderMaster);

            $sts = $this->db->insert_id();
            if ($sts) {

                $track = array(
                    'order_id' => $sts,
                    'restaurant_id' => $cartDetails->restaurant_id,
                    'status' => 1,
                );
                $this->db->insert('track_order', $track);

                $this->sendNotification($token, $sts, $cartDetails->restaurant_id);
                return True;
            } else {
                $this->db->set('is_order_placed', 0)->set('cart_master_status', 'Added to cart')->set('paymentmethod', '')->where('cart_master_id', $cartId)->update('cart_master');
                return false;
            }
        } else {
            $this->db->set('is_order_placed', 0)->set('cart_master_status', 'Added to cart')->set('paymentmethod', '')->where('cart_master_id', $cartId)->update('cart_master');
        }
    }

    function updateCouponPoints($userId, $couponcode)
    {
        if ($couponcode) {
            $updateCoupons = $this->db->select('coupon_code_value,balance_points')->where('coupon_code_name', $couponcode)->where('user_id', $userId)->get('coupon_code')->row();

            if ($couponcode) {
                $this->db->set('is_active', 1)->where('coupon_code_name', $couponcode)->update('coupon_code');

                $this->db->set('available_points', $updateCoupons->balance_points)->where('user_id', $userId)->update('users');
            }
        }
    }

    function sendNotification($tokens, $orderId, $restaurantId)
    {
        $tkn = $this->db->select('token_id')->where('restaurant_id', $restaurantId)->where('is_active', 1)->get('restaurant_token')->row();
        $tokens =  $tkn->token_id;
        date_default_timezone_set('Asia/Kolkata');
        $dateNow = date("Y-m-d H:i:s");
        $url = 'https://fcm.googleapis.com/fcm/send';
        // $notificationData = $this->getNotificationData($orderId);
        $fields = array(
            "to" => $tokens,
            "notification" => array(
                "title" => $title = 'New Order', "body" => $message = 'new order placed',
                "notificationType" => $notificationType = 1, "orderId" => $orderId, 'date' => $dateNow, 'orderStatusId' => 'Order recieved'
            )
            //'data' => $message			
        );

        $headers = array(
            'Authorization:key = AAAABRPwCC8:APA91bHU0yEAfgiA6HgLcxvYcg1Yinaodr5sMTT7W_ijteabutxrZ39n6ihpXruD5RMz47LtSuFt6FEl76e5yDwdln5TzY2luZrVZD8zmo_S4xI_aZ384QIpSly7GA-9fij4IV3QczzO',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // print_r($result);
        curl_close($ch);

        // return $result;
    }

    public function generateCoupon($oamt, $userId)
    {
        $points = $this->db->select('available_points as available_view_cnt')->where('user_id', $userId)->where('is_deleted', 0)->get('users')->row();
        if ($points) {
            $cpnstngs = $this->db->get('coupon_settings')->row();

            if ($oamt >= $cpnstngs->minimun_amount_required) {
                if ($points->available_view_cnt >= $cpnstngs->minimum_points_required) {
                    $totalpoints = $points->available_view_cnt;
                    $onePointValue = $cpnstngs->one_point_value;


                    $cpnAmt1 = '';
                    $cpnAmt2 = '';
                    $couponvalue =  $totalpoints * $onePointValue;

                    if ($couponvalue > $oamt) {

                        $cvalue = $oamt / $onePointValue;
                        $cpnAmt1 = $cvalue * $onePointValue;
                        $balancePoint = $totalpoints - $cvalue;
                        $percentage =  ($oamt / $cpnAmt1) * 100;
                        $data = $this->createCouponCode($percentage, $balancePoint, $userId, $cpnAmt1);
                        return   array('status' => Successfull, 'result' =>  $data);
                    } else {
                        $cpnAmt1 = $totalpoints * $onePointValue;
                        $balancePoint = $totalpoints - $totalpoints;
                        $percentage =  ($cpnAmt1 / $oamt) * 100;
                        $data = $this->createCouponCode($percentage, $balancePoint, $userId, $cpnAmt1);
                        return  array('status' => Successfull, 'result' =>  $data);
                    }
                } else {
                    return array('status' => Successfull, 'result' => null, 'message' => "minimum points to generate coupon code: " . $cpnstngs->minimum_points_required);

                    // return "minimum points to generate coupon code: " . $cpnstngs->minimum_points_required;
                }
            } else {
                return  array('status' => Successfull, 'result' => null, 'message' => "minimum order amount to generate coupon code: " . $cpnstngs->minimun_amount_required);
            }
        } else {


            return  array('status' => Successfull, 'result' => null, 'message' => "User Does not have Points ");
        }
    }

    function createCouponCode($percentage, $balancePoint, $userId, $cpnAmt1)
    {
        // $this->load->helper('string');
        $rnd = rand(10, 100);


        $cpncde = 'Foodo#' . $userId . $rnd . date('d') . date('s');
        $arr = array(
            'coupon_code_name' =>  $cpncde,
            'coupon_code_value' => $cpnAmt1,
            'coupon_code_percentage' => $percentage,
            'user_id' => $userId,
            'balance_points' => $balancePoint,
        );
        $this->db->insert('coupon_code', $arr);
        $id = $this->db->insert_id();
        return  $this->db->select('coupon_code_id,coupon_code_name as couponcode,coupon_code_percentage as percentage')->where('coupon_code_id', $id)->get('coupon_code')->row();
    }

    public function applyCoupon($userId, $cartId, $latitude, $longitude, $restaurantId, $couponcode)
    {
        // get distance 
        $res_val =  $this->db->select('latitude,longitude')->where('restaurant_id', $restaurantId)->get('restaurants')->row();

        $res_lati = trim($res_val->latitude);

        $res_longi = trim($res_val->longitude);


        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

        $imp = explode(' ', $dist);


        if ($imp[1] == 'km') {
            $distances = floor($imp[0]);
        } else {
            $distances  = 1;
        }

        $wre = " $distances between minimum_distance and maximum_distance";
        $deliverycharge =  $this->db->select('delivery_charge')->where($wre)->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->get('deliverycharge_distance')->row();

        $couponcodePrice = $this->db->select('coupon_code_value')->where('coupon_code_name', $couponcode)->where('user_id', $userId)->get('coupon_code')->row();

        $cmaster = $this->db->select('coupon,is_coupon_applied,total_amount')->where('cart_master_id', $cartId)->get('cart_master')->row();

        if ($cmaster->is_coupon_applied == 0) {
            $this->db->query("UPDATE `cart_master` SET total_amount = total_amount - $couponcodePrice->coupon_code_value,coupon = '$couponcode',`is_coupon_applied` = 1 WHERE `cart_master_id` = $cartId");
        }
        $rslt =   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId,delivery_charge,amount,delivery_charge')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();

        return array('grandtotal' => $rslt->payable, 'itemTotal' => $rslt->amount, 'tax' => $rslt->tax, 'cartId' => $rslt->cartId, 'distance' => $dist, 'delivery' => $rslt->delivery_charge);

        // return   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();
    }


    public function removeCoupon($userId, $cartId, $latitude, $longitude, $restaurantId, $couponcode)
    {
        // get distance 
        $res_val =  $this->db->select('latitude,longitude')->where('restaurant_id', $restaurantId)->get('restaurants')->row();

        $res_lati = trim($res_val->latitude);

        $res_longi = trim($res_val->longitude);


        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

        $imp = explode(' ', $dist);


        if ($imp[1] == 'km') {
            $distances = floor($imp[0]);
        } else {
            $distances  = 1;
        }

        $wre = " $distances between minimum_distance and maximum_distance";
        $deliverycharge =  $this->db->select('delivery_charge')->where($wre)->where('restaurant_id', $restaurantId)->where('is_deleted', 0)->get('deliverycharge_distance')->row();

        $couponcodePrice = $this->db->select('coupon_code_value')->where('coupon_code_name', $couponcode)->where('user_id', $userId)->get('coupon_code')->row();

        $cmaster = $this->db->select('coupon,is_coupon_applied,total_amount')->where('cart_master_id', $cartId)->get('cart_master')->row();

        if ($cmaster->is_coupon_applied > 0) {
            $this->db->query("UPDATE `cart_master` SET total_amount = total_amount + $couponcodePrice->coupon_code_value,coupon = '',`is_coupon_applied` = 0 WHERE `cart_master_id` = $cartId");
        }
        $rslt =   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId,delivery_charge,amount,delivery_charge')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();

        return array('grandtotal' => $rslt->payable, 'itemTotal' => $rslt->amount, 'tax' => $rslt->tax, 'cartId' => $rslt->cartId, 'distance' => $dist, 'delivery' => $rslt->delivery_charge);

        // return   $this->db->select('total_amount as payable,tax_amount as tax,cart_master_id as cartId')->where('customer_id', $userId)->where('cart_master_id', $cartId)->get('cart_master')->row();
    }


    private function createOrderRazorPay($orderAmount, $orderId)
    {

        $fields = array('amount' => $orderAmount * 100, 'currency' => 'INR', 'receipt' => strval($orderId));
        $headers = array(

            'Content-Type: application/json'
        );
        $url = 'https://api.razorpay.com/v1/orders';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERPWD, "rzp_test_7CrlaSDwSgMxAk:WtXUwLEENNc5ybeIHK9m1SQP");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }


        curl_close($ch);
        return  json_decode($result);
    }

    public function bankCheckOut($userId, $cartId, $latitude, $longitude, $restaurantId, $date, $timeSlotId, $token, $deliveryModeId)
    {

        $uData =  $this->db->select('first_name,mobile,email')->where('id', $userId)->get('users')->row();
        if ($deliveryModeId == 2) {
            $delryCrge =  $this->db->select('delivery_charge')->where('cart_master_id', $cartId)->get('cart_master')->row();
            if ($delryCrge->delivery_charge > 0) {
                $this->db->query("UPDATE `cart_master` SET total_amount = total_amount - delivery_charge,cart_master_status = 1,`paymentmethod` = 'Bank',`delivery_charge` = 0,`is_order_placed` = 1 WHERE `cart_master_id` = $cartId");
            }
        } else {
            $this->db->set('is_order_placed', 1)->set('cart_master_status', 1)->set('paymentmethod', 'Bank')->where('cart_master_id', $cartId)->update('cart_master');
        }

        $updateSts = $this->db->affected_rows();

        if ($updateSts > 0) {
            $cartDetails = $this->db->where('cart_master_id', $cartId)->get('cart_master')->row();

            $deliveryAddress = $this->db->where('user_id', $userId)->where('is_default', 1)->where('is_deleted', 0)->get('users_address')->row();
            $this->updateCouponPoints($userId, $cartDetails->coupon);
            $orderMaster = array(
                'cart_master_id' => $cartId,
                'deliveryMode' => $deliveryModeId,
                'restaurant_id' => $cartDetails->restaurant_id,
                'customer_id' => $userId,
                'order_amount' => $cartDetails->total_amount,
                'refund_amount' => 0,
                'order_status' => 0,
                // 'order_status' => $cartDetails->cart_master_status,
                'payment_status' => 0,
                'payment_transaction_id' => 0,
                'razorpay_orderId' => 0,
                'delivery_address' => $deliveryAddress->address,
                'address_id' => $deliveryAddress->users_address_id,
                'delivery_time_from ' => 0,
                'delivery_time_to' => 0,
                'time_slot' => $timeSlotId,
                'delivery_date' => $date,
                'delivery_charge' => $cartDetails->delivery_charge,
                'tax_amount' => $cartDetails->tax_amount,
                'coupon_code' => isset($cartDetails->coupon) ? $cartDetails->coupon : 0,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'bag_num' => 0,
                'payment_method' => isset($cartDetails->paymentmethod) ? $cartDetails->paymentmethod : 0,
            );

            $this->db->insert('order_master', $orderMaster);

            $sts = $this->db->insert_id();

            $razorPayresponse = $this->createOrderRazorPay($cartDetails->total_amount, $sts);

            if ($sts) {
                $this->db->set('razorpay_orderId', $razorPayresponse->id)->where('order_master_id', $sts)->update('order_master');

                return array('orderId' => $sts, 'razorpayId' => $razorPayresponse->id, 'amount' => $razorPayresponse->amount, 'currency' => $razorPayresponse->currency, 'cusName' => $uData->first_name, 'mobile' => $uData->mobile, 'email' => $uData->email);
                // $track = array(
                //     'order_id' => $sts,
                //     'restaurant_id' => $cartDetails->restaurant_id,
                //     'status' => 1,
                // );
                // $this->db->insert('track_order', $track);

                // $this->sendNotification($token, $sts);
                // return True;
            } else {
                $this->db->set('is_order_placed', 0)->set('cart_master_status', 'Added to cart')->set('paymentmethod', '')->where('cart_master_id', $cartId)->update('cart_master');
                return false;
            }
        } else {
            $this->db->set('is_order_placed', 0)->set('cart_master_status', 'Added to cart')->set('paymentmethod', '')->where('cart_master_id', $cartId)->update('cart_master');
        }
    }

    public function orderHistory($id)
    {
        $orders =  $this->db->select('order_master.order_amount,order_master.restaurant_id,order_master.order_master_id,order_master.delivery_date,order_master.created_date,restaurants.restaurant_name,restaurants.district,restaurants.city,district.district_name,city.city_name,fud_sts_master.fud_status')->join('restaurants', 'restaurants.restaurant_id = order_master.restaurant_id', 'inner')->join('district', 'district.district_id = restaurants.district', 'inner')->join('city', 'city.city_id = restaurants.city', 'inner')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = order_master.order_status', 'inner')->where('order_master.customer_id', $id)->order_by('order_master.order_master_id', 'desc')->get('order_master')->result();
        $history = [];
        foreach ($orders as $keys => $ordsval) {
            $obj = new stdClass();
            $obj->orderNumber = $ordsval->order_master_id;
            $obj->dateOfOrder = date('Y-m-d', strtotime($ordsval->created_date));
            $obj->restaurantName = $ordsval->restaurant_name;
            $obj->branch = $ordsval->district_name . ',' . $ordsval->city_name;
            $obj->orderStatus = $ordsval->fud_status;
            $obj->grandTotal = $ordsval->order_amount;
            $history[] = $obj;
        }
        return  $history;
    }

    public function orderDetails($userId, $orderId)
    {
        $details =   $this->db->select('order_master.cart_master_id,cart_details.food_name as varientName,cart_details.quantity,cart_details.selling_price,cart_details.message,food_units.master_id,food_master.item_name,food_master.food_image')->join('cart_details', 'cart_details.cart_master_id = order_master.cart_master_id', 'inner')->join('food_units', 'food_units.unit_id = cart_details.food_unit_id', 'inner')->join('food_master', 'food_master.master_id = food_units.master_id', 'inner')->where('order_master.customer_id', $userId)->where('order_master.order_master_id', $orderId)->order_by('order_master.order_master_id', 'asc')->get('order_master')->result();
        $dtlsar = [];
        foreach ($details as $key => $dval) {
            $dtls = new stdClass();
            $dtls->image = base_url() . $dval->food_image;
            $dtls->price = (int)$dval->selling_price;
            $dtls->foodName = $dval->item_name;
            $dtls->varientName = $dval->varientName;
            $dtls->quantity = $dval->quantity;
            $dtlsar[] = $dtls;
        }
        return $dtlsar;
    }

    public function trackMyOrder($userId, $orderId)
    {
        $order =   $this->db->select('track_order.status,fud_sts_master.image,track_order.created_at,fud_sts_master.fud_status')->join('fud_sts_master', 'fud_sts_master.fud_sts_master_id = track_order.status', 'inner')->where('track_order.order_id', $orderId)->order_by('track_order.track_order_id', 'asc')->get('track_order')->result();
        $dtme = [];
        foreach ($order as $k => $ovl) {
            $ordrObj = new stdClass();
            $dte = date('Y-m-d', strtotime($ovl->created_at));
            $time = date('H:i', strtotime($ovl->created_at));

            $first_date = date('d-m-Y', strtotime($ovl->created_at));
            $dayofweek = date('l', strtotime($dte));
            $timeS = date('h:i A', strtotime($time));
            $ordrObj->image =  base_url() . $ovl->image;
            $ordrObj->status =  $ovl->fud_status;
            $ordrObj->datetime = $first_date . ' ' . $dayofweek . ' , Time ' . $timeS;
            $dtme[] =  $ordrObj;
        }
        return  $dtme;
    }

    public function  razorresponse($payment_id, $razorpay_order_id, $signature, $order_id)
    {

        $payd = array(
            'payment_id' => $payment_id,
            'razorpay_order_id' => $razorpay_order_id,
            'order_id' => $order_id,
            'signature' => $signature,
        );
        $this->db->insert('razorpay_response', $payd);

        $masterRazorPayOrderId = $this->db->select('razorpay_orderId')->where('order_master_id', $order_id)->get('order_master')->row();

        $masterRazorPayOrderId  = $masterRazorPayOrderId->razorpay_orderId;
        // echo $masterRazorPayOrderId;
        $razorpayPaymentId = $payment_id;
        $key_secret = "rzp_test_7CrlaSDwSgMxAk:WtXUwLEENNc5ybeIHK9m1SQP";
        // $generated_signature = hash_hmac('sha256', $masterRazorPayOrderId + "|" + $razorpayPaymentId, $key_secret, $as_binary = true);
        $generated_signature = hash_hmac('sha256', $masterRazorPayOrderId + "|" + $razorpayPaymentId, $key_secret);
        //$hmac = crypto . createHmac('sha256', RAZORPAY_KEY_SECRET);
        //hash('sha256', $key_secret);
        //$generated_signature = hmac_sha256($masterRazorPayOrderId + "|" + $razorpayPaymentId, $key_secret);
        // print_r($generated_signature);
        if ($generated_signature == $signature) {
            return true;
        } else {
            return false;
        }
    }

    public function checkCustomerDistance($restaurantId, $userId)

    {
        $resdsat =  $this->db->select('restaurants.latitude,restaurants.longitude,settings.maximum_delivery_distance')->join('settings', 'settings.restaurant_id = restaurants.restaurant_id', 'inner')->where('restaurants.restaurant_id', $restaurantId)->get('restaurants')->row();

        $address = $this->db->select('users_address.latitude,users_address.longitude')->where('user_id', $userId)->where('is_default', 1)->get('users_address')->row();

        $ulat = trim($address->latitude);
        $ulong =  trim($address->longitude);

        $res_lati = trim($resdsat->latitude);
        $res_longi = trim($resdsat->longitude);
        $delDistance = $resdsat->maximum_delivery_distance;



        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$ulat,$ulong&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

        // https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=11.25923058783468,75.78058261301749&destinations=11.297482400283902,75.82201116382896&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

        $imp = explode(' ', $dist);

        if ($imp) {
            if ($imp[1] == 'km') {
                $distances = floor($imp[0]);
            } else {
                $distances  = 1;
            }

            if ($distances > $delDistance) {
                return $delDistance;
            } else {
                return 0;
            }
        } else {
            return 100;
        }
    }

    public function getMinimumOrderAmount($restaurantId)
    {
        return  $this->db->select('minimum_order_amount')->where('restaurant_id', $restaurantId)->get('settings')->row();
    }
}
