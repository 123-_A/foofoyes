<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['api/signupFb']     = 'api/registration/signupFb';
$route['api/signupGoogle']     = 'api/registration/signupGoogle';
$route['api/signupWithMob']     = 'api/registration/signup';
$route['api/verifyOtp']     = 'api/registration/varifyOtp';
$route['api/updateProfile']     = 'api/registration/updateProfile';
$route['api/loginWithOtp']     = 'api/registration/loginWithOtp';
$route['api/resendOtp']     = 'api/registration/resendOtp';
$route['api/deleteUserByMob']     = 'api/registration/deleteUserByMob';
$route['api/loginWithPassword']     = 'api/registration/loginWithPassword';
$route['api/forgotPassword']     = 'api/registration/forgotPassword';
$route['api/updatePassword']     = 'api/registration/updatePassword';
$route['api/addUserFiles']     = 'api/Home/uploadUserFiles';
$route['api/getUserFiles']     = 'api/Home/getUserFiles';
$route['api/deleteUserFileById']     = 'api/Home/deleteUserFileById';
$route['api/homeData']     = 'api/Home/homeData';
$route['api/addAddress']     = 'api/Home/addAddress';
$route['api/editAddress']     = 'api/Home/editAddress';
$route['api/deleteAddress']     = 'api/Home/deleteAddress';
$route['api/getHashtags']     = 'api/Home/getHashtags';
$route['api/deliveryAddress']     = 'api/Home/userDeliveryAddress';
$route['api/restaurant']     = 'api/restaurant/restaurants';
$route['api/listCategory']     = 'api/restaurant/listCategory';
$route['api/listCategoryById']     = 'api/restaurant/listCategoryById';

$route['api/add-firebasetoken']     = 'api/Login/addUserFirebaseDetails';
$route['api/remove-firebasetoken']     = 'api/Login/deleteUserFirebaseDetails';

// Api cart
$route['api/cart']     = 'api/cart/cart';
$route['api/cod']     = 'api/cart/cod';
$route['api/bank']     = 'api/cart/bank';
$route['api/codCheckOut']     = 'api/cart/codCheckOut';
$route['api/generateCoupon']     = 'api/cart/generateCoupon';
$route['api/applyCoupon']     = 'api/cart/applyCoupon';
$route['api/removeCoupon']     = 'api/cart/removeCoupon';
$route['api/orderHistory']     = 'api/cart/orderHistory';
$route['api/orderDetails']     = 'api/cart/orderDetails';
$route['api/trackMyOrder']     = 'api/cart/trackMyOrder';
$route['api/bankCheckOut']     = 'api/cart/bankCheckOut';
$route['api/razorResponse']     = 'api/cart/razorresponse';
$route['api/checkCustomerDistance']     = 'api/cart/checkCustomerDistance';



// Admin Routes
$route['login']     = 'admin/login';
$route['home']     = 'admin/home';
$route['addcity']     = 'admin/city';
$route['food_category']     = 'admin/food_category';
$route['varient']     = 'admin/food_category/varient';
$route['adpromotion']     = 'admin/Adpromotion';
$route['delivery_partner']     = 'admin/Delivery_partner';
$route['restuarant']     = 'admin/Restaurant';
$route['recentpost']     = 'admin/Recentpost';
$route['coupon']     = 'admin/Coupon';
$route['default_controller'] = 'login';



// Restaurant Routes
$route['restaurant']     = 'restaurant/login';
$route['restaurant/home']     = 'restaurant/home';
$route['restaurant/myarea']     = 'restaurant/home/myarea';
$route['restaurant/fudlist/(:any)']     = 'restaurant/home/listFoods/$0';
$route['restaurant/settings']     = 'restaurant/home/settings';
$route['restaurant/timeslot']     = 'restaurant/home/timeslot';
$route['restaurant/foodtime']     = 'restaurant/home/foodtime';
$route['restaurant/charge_distance']     = 'restaurant/home/charge_distance';
$route['restaurant/edit/(:any)']     = 'restaurant/home/editfudlist/$1';

$route['restaurant/vieworder/(:any)']     = 'restaurant/order/viewOrder/$1';
// $route['addcity']     = 'admin/city';
// $route['food_category']     = 'admin/food_category';
// $route['varient']     = 'admin/food_category/varient';
// $route['adpromotion']     = 'admin/Adpromotion';
// $route['delivery_partner']     = 'admin/Delivery_partner';
// $route['restuarant']     = 'admin/Restaurant';
// $route['recentpost']     = 'admin/Recentpost';
// $route['default_controller'] = 'login';
