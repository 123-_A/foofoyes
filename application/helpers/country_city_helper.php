<?php


function loadcountry()
{

    $ci = &get_instance();
    $ci->load->model('admin/city_model', 'cmodel');
    return  $ci->cmodel->getAllCountrys();
}


function getStateNameByCntryId($cntryid)
{
    $ci = &get_instance();
    $ci->load->model('admin/city_model', 'cmodel');
    return  $ci->cmodel->getStateNameByCntryId($cntryid);
}
function getCountryNameById($id)
{
    $ci = &get_instance();
    $ci->load->model('admin/city_model', 'cmodel');
    return  $ci->cmodel->getCountryNameById($id);
}

function getCityNameByDistrictId($disId)
{
    $ci = &get_instance();
    $ci->load->model('admin/city_model', 'cmodel');
    return  $ci->cmodel->getCityNameByDistrictId($disId);
}
