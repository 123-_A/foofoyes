<?php
$ci = &get_instance();
$ci->load->model('Common_model', 'cmodel');
$pname = $ci->cmodel->getRestaurantNameById($_SESSION['uid']);
$fltr = strtoupper($pname[0]);

?>

<div class="slide_bar">
    <a href="<?= base_url() ?>restaurant/home">
        <div class="slide_bar_inner_top"><img src="<?= base_url() ?>resources/images/logo.jpg" alt="" /></div>
    </a>
    <div class="slide_bar_inner">
        <div class="slide_bar_inner_in">
            <ul>

                <a href="<?= base_url() ?>restaurant/home">
                    <li class="active"><i class="fas fa-th"></i>
                        <h1>Dashboard</h1>
                    </li>
                </a>
                <a href="<?= base_url() ?>restaurant/order/order">
                    <li><i class="fas fa-shopping-bag"></i>
                        <h1>my orders</h1>
                    </li>
                </a>
                <a href="<?= base_url('restaurant/home/listFoods'); ?>">
                    <li><i class="fas fa-utensils"></i>
                        <h1>our menu</h1>
                    </li>
                </a>
                <a href="<?= base_url() ?>restaurant/foodtime">
                    <li><i class="fas fa-clock"></i>
                        <h1>food time</h1>
                    </li>
                </a>
                <a href="<?= base_url() ?>restaurant/charge_distance">
                    <li><i class="fas fa-map-marker-alt"></i>
                        <h1>delivery charge & Distance</h1>
                    </li>
                </a>
                <!-- <a href="<?= base_url() ?>restaurant/myarea">
                    <li><i class="fas fa-map-marker-alt"></i>
                        <h1>My area</h1>
                    </li>
                </a> -->
                <a href="<?= base_url() ?>restaurant/timeslot">
                    <li><i class="fas fa-map-marker-alt"></i>
                        <h1>Time Slot</h1>
                    </li>
                </a>
                <a href="<?= base_url() ?>restaurant/settings">
                    <li><i class="fas fa-cogs"></i>
                        <h1>settings</h1>
                    </li>
                </a>


            </ul>
            <div class="clear"></div>
        </div>


    </div>
</div>

<div class="dashboard_right">

    <div class="head">
        <div class="profile_name">
            <div class="profile_name_inn">
                <ul>
                    <li>
                        <a href="<?= base_url() ?>restaurant/login/logout">
                            <p> Logout <span></span></p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p> order notification <span><i class="fas fa-bell"></i></span></p>
                        </a>
                    </li>
                    <li><a href="#">
                            <div class="profile_name_user">
                                <div class="profile_name_user_left">
                                    <p>hello <?= isset($pname) ? $pname  : ''; ?></p>
                                </div>
                                <div class="profile_name_user_right">
                                    <div class="profile_name_user_right_in">
                                        <h1><?= $fltr; ?></h1>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>

        <div class="quote">