<style>
    .button_reject_new {
        padding: 1em 30px;
        text-align: center;
        width: auto;
        margin: 0 auto;
        display: block;
        -webkit-transition: all linear 0.15s;
        transition: all linear 0.15s;
        border-radius: 5px;
        background: red;
        font-size: 17px;
        text-decoration: none;
        border: none;
        letter-spacing: 1px;
        text-transform: uppercase;
        color: #FFFFFF !important;
        font-family: 'Oswald', sans-serif;
        font-weight: bold !important;
    }

    /* pop up style */

    .box_new_popup {
        width: auto;
        margin: 0 auto;
        background: no-repeat;
        padding: 0;
        /* border: 2px solid red; */
        position: relative;
        text-align: center;
        z-index: 999;
    }

    .button_new_popup {
        font-size: 0;
        padding: 0;
        color: #fff;
        text-decoration: none;
        float: right;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }

    .button_new_popup:hover {
        background: none;
    }

    .overlay_new_popup {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
        z-index: 1000;
    }

    .overlay_new_popup:target {
        visibility: visible;
        opacity: 1;
    }

    .popup_new_popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 50%;
        position: relative;
        /* transition: all 5s ease-in-out; */
    }

    .popup_new_popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup_new_popup .close {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #000;
        background: red;
        /* border-radius: 50%; */
        padding: 1% 2%;
    }

    .popup_new_popup .close:hover {
        color: #fff;
        background: red !important;
    }

    .popup_new_popup .content {
        max-height: 50%;
        overflow: auto;
    }

    @media screen and (max-width: 700px) {
        .box_new_popup {
            width: 70%;
        }

        .button_new_popup {
            font-size: 25px;
            color: red;
            padding: 25%;
            border: red solid 1px;
            border-radius: 6px;
            overflow: hidden;
        }
    }



    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button .active {
        background-color: #FFFFFF;
    }

    .active {
        color: #FFF !important;
        background-color: #f15733 !important;
        border-color: #000 solid 1px !important;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 17px;
        padding-right: 17px;
        border-radius: 6px;
    }


    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination ul {
        margin: 0 0 20px;
        padding: 0;
        list-style: none;
        text-align: center;
    }

    .pagination li {
        display: inline-block;
        margin-right: 5px;
    }

    .pagination li a,
    .pagination li span {
        color: #ffffff;
        padding: 14px 20px;
        font-family: 'Oswald', sans-serif;
        font-weight: bold;
        text-decoration: none;
        border-radius: 6px;
        background-color: #f15733;
        border-color: #000;
    }

    .pagination li a .active {
        background-color: #FFF !important;
        border-color: #000 solid 1px !important;
    }

    .pagination li a .current {
        background-color: #FFF !important;
        border-color: #000 solid 1px !important;
    }
</style>

<div class="popup_main">
    <div class="obscure">
        <div class="popup animationClose">

            <div class="contact_form">
                <form action="mailsend.php" method="post" id="foodform">

                    <div class="items_display">
                        <div class="items_display_firstline">
                            <ul>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>item name <span>*</span></label>
                                        <input name="itemName" class="itemName_cls" type="text" id="#" placeholder="Enter Item Name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>Discription <span>*</span></label>
                                        <input name="description" class="description_cls" type="text" id="#2" placeholder="Enter Discription" required />
                                        <span id="#3" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>category <span>*</span></label>
                                        <select name="fudcats" id="#123" class="items_display_firstline_dropdown fudcats_cls">
                                            <option value="">Select Category</option>
                                            <?php if ($listresults['food_category']) {
                                                foreach ($listresults['food_category'] as $key => $cats) {
                                            ?>
                                                    <option value="<?= $cats->food_category_id ?>"><?= $cats->food_category_name ?></option>
                                            <?php }
                                            } ?>

                                        </select>
                                    </div>
                                </li>
                                <!-- <li>
                                    <div class="items_display_firstline_inn">
                                        <label>display price <span>*</span></label>
                                        <input name="disprice" class="disprice_cls" type="text" id="#6" placeholder="Enter Display Price" required />
                                        <span id="#7" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>Offer Price <span>*</span></label>
                                        <input name="ofrprice" type="text" class="ofrprice_cls" id="#8" placeholder="Enter Offer Price" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li> -->
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>Tax <span>*</span></label>
                                        <select name="fudtax" id="#123" class="items_display_firstline_dropdown fudtax_cls">
                                            <option value="">Select Category</option>
                                            <?php if ($listresults['tax']) {
                                                foreach ($listresults['tax'] as $key => $cats) {
                                            ?>
                                                    <option value="<?= $cats->tax_amt ?>"><?= $cats->tax_amt ?></option>
                                            <?php }
                                            } ?>

                                        </select>

                                    </div>
                                </li>

                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>Type <span>*</span></label>
                                        <select name="fud_types" id="#121" class="items_display_firstline_dropdown fud_types_cls">
                                            <option value="" selected>Select Type</option>
                                            <option value="veg">Veg</option>
                                            <option value="non-veg">Non-Veg</option>

                                        </select>

                                    </div>
                                </li>


                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>Group <span>*</span></label>
                                        <select name="fudgrp" id="#123" class="items_display_firstline_dropdown fudgrp_cls">
                                            <option value="">Select Category</option>
                                            <?php if ($listresults['food_groups']) {
                                                foreach ($listresults['food_groups'] as $key => $cats) {
                                            ?>
                                                    <option value="<?= $cats->foodgroupId ?>"><?= $cats->food_group_name ?></option>
                                            <?php }
                                            } ?>

                                        </select>


                                    </div>
                                </li>

                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>image <span>*</span></label>
                                        <input type="file" class="myfiles_cls" id="myFile" name="myfiles" required />
                                        <span id="#12" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>thumb <span>*</span></label>
                                        <input type="file" id="myFile2" class="thumpfile_cls" name="thumpfile" required />
                                        <span id="#13" class="spn_Error" style="display:none;"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="items_display_firstline_inn">
                                        <label>custom message <span>*</span></label>
                                        <label class="switch">
                                            <input type="checkbox" name="chngecusmsg" value="0" class="chngecusmsg_cls">
                                            <!-- checked -->
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>


                            </ul>


                            <div class="clear"></div>
                        </div>


                        <div class="add_food_btn">
                            <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                            <div class="add_food_btn_right"><a href="#"><i class="fas fa-times"></i></a></div>
                            <div class="clear"></div>
                        </div>
                    </div>





                </form>
            </div>

            <a class="closeBtn" href="#"></a>
        </div>
    </div>
    <a class="openBtn" href="#">add a new food</a>
</div>
</div>



<div class="clear"></div>
</div>
















<div class="dashboard_right_inner">
    <div class="dashboard_right_inner_intop">
        <ul>
            <li class="dashboard_right_inner_intop_c1">
                <h1>total</h1>
                <h2>orders</h2>
                <h3>2,256 orders</h3>
            </li>
            <li class="dashboard_right_inner_intop_c2">
                <h1>delivered</h1>
                <h2>orders</h2>
                <h3>2,256 orders</h3>
            </li>
            <li class="dashboard_right_inner_intop_c3">
                <h1>todays</h1>
                <h2>revenew</h2>
                <h3>2,256 Rs</h3>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="recent_order">
    <a href="my-order.html">
        <div class="recent_order_left">
            <h1>recent orders</h1>
        </div>
    </a>
    <div class="recent_order_right">
        <div class="recent_order_right_left">
            <input type="text" class="search-place" placeholder="Search">
        </div>
        <div class="recent_order_right_right"><a href="#"><img src="" alt="" /></a></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>




<div class="list_order">
    <div class="list_order_inner">
        <ul>
            <?php if ($listorders) {
                foreach ($listorders as $key => $lisVal) { ?>


                    <li>
                        <div class="list_order_top">
                            <div class="list_order_top_left">
                                <h1>order #<?= $lisVal->orderId; ?><span><a href="<?= base_url() ?>restaurant/vieworder/<?= base64_encode($lisVal->orderId) ?>">- view details</a></span> </h1>
                                <p><?php
                                    $date = date_create($lisVal->createdDate);
                                    echo date_format($date, "d F Y g:i a");
                                    ?></p>
                            </div>
                            <div class="list_order_top_right">
                                <div class="list_order_top_right_in"><img src="images/user.jpg" alt="" /></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="list_order_centerscroll">
                            <?php foreach ($lisVal->foodItems as $items) : ?>
                                <div class="list_order_centerscroll_list">
                                    <div class="list_order_centerscroll_list_left">
                                        <h1><?= $items->item_name ?> <?= $items->food_name ?></h1>
                                        <p>Qty - <?= $items->quantity ?></p>
                                    </div>
                                    <div class="list_order_centerscroll_list_right">
                                        <h1>rs <?= $items->selling_price ?></h1>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            <?php endforeach; ?>


                        </div>
                        <div class="list_order_bottom">
                            <div class="list_order_bottom_left">
                                <h1><?= $lisVal->countOfItems; ?> items</h1>
                                <h2> Rs <?= number_format($lisVal->order_amount); ?> </h2><small>(Include tax of Rs <?= $lisVal->tax; ?>)</small>
                            </div>
                            <div class="list_order_bottom_right">
                                <?php if ($lisVal->order_status_val == '1') { ?>
                                    <h2><?= $lisVal->order_status ?>
                                        <div class="list_order_bottom_right">
                                            <div class="list_order_bottom_right_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                            <div class="list_order_bottom_right_right"><a href="#" class=""><i class="fas fa-times"></i></a></div>
                                            <div class="list_order_bottom_right_right">
                                                <div id="popup1" class="overlay_new_popup">
                                                    <div class="popup_new_popup">
                                                        <h2>Here i am</h2>
                                                        <a class="popup_new_popup" href="#">&times;</a>
                                                        <div class="content">
                                                            Thank to pop me out of that button, but now i'm done so you can close this window.
                                                        </div>
                                                    </div>
                                                </div>

                                                </i></a>
                                            </div>
                                            <div class="box_new_popup">
                                                <a class="button_new_popup" href="#popup1">Let me Pop up</a>
                                            </div>


                                            <div class="clear"></div>
                                        </div>

                                    <?php } else { ?> <h2><?= $lisVal->order_status ?></h2> <?php } ?>
                                    <div class="clear"></div>
                            </div>

                            <div class="clear"></div>

                        </div>

                    </li>


                <?php }  ?>



            <?php
            } else { ?>
                <center>
                    <h3 style="margin-top: 200px;">NO ORDERS FOUND</h3>
                </center>;
            <?php   } ?>


        </ul>
    </div>

    <div class="clear"></div>
    <audio id="myAudio">

        <source src="<?= base_url() ?>assets/orderaudio/femalevoice.mp3" autoplay="autoplay">
    </audio>

</div>
<?php echo $this->pagination->create_links(); ?>
</div>
<!-- <script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-messaging.js" type="text/javascript"></script> -->
<!-- <script src="../firebase-messaging-sw.js" type="text/javascript"></script> -->

<script src="<?= base_url() ?>res_resources/js/main_jQuery.js" type="text/javascript"></script>
<script>
    // $(function() {
    //     var x = document.getElementById("myAudio");
    //     x.load();
    //     x.play();
    // });
</script>
<script>
    $('.chngecusmsg_cls').click(function() {
        var _sts = $(this).val();
        if (_sts == 0) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });


    $('.add_food_btn_left').click(function() {

        var formData = new FormData($('#foodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/newfud',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('Food added');
                    location.reload(true);

                } else {
                    alert('Failed to add food');
                }
            }
        });
    });
</script>


<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-messaging.js"></script>
<!-- <script src=""></script> -->
<!-- <script>
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('../../firebase-messaging-sw.js');
        });
    }
</script> -->

<script>
    var firebaseConfig = {
        apiKey: "AIzaSyAxZsHCyp0CqMhk3vhNlsZmSLixxaKlPyk",
        authDomain: "foodoyes-2644c.firebaseapp.com",
        projectId: "foodoyes-2644c",
        storageBucket: "foodoyes-2644c.appspot.com",
        messagingSenderId: "21809334319",
        appId: "1:21809334319:web:2acc40b82cf34388aededa",
        measurementId: "G-Y4FEBPLLH1"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const messaging = firebase.messaging();

    function IntitalizeFireBaseMessaging() {
        messaging
            .requestPermission()
            .then(function() {
                console.log("Notification Permission");
                return messaging.getToken();
            })
            .then(function(token) {
                // console.log("Tokenx : " + token);
                addNewUserToken(token);
                document.getElementById("token").innerHTML = token;
            })
            .catch(function(reason) {
                console.log(reason);
            });
    }

    messaging.onMessage(function(payload) {



        var x = document.getElementById("myAudio");
        x.load();
        x.play();



        const notificationOption = {
            body: payload.notification.body,
            icon: payload.notification.icon
        };

        if (Notification.permission === "granted") {
            var notification = new Notification(payload.notification.title, notificationOption);

            notification.onclick = function(ev) {
                ev.preventDefault();
                window.open(payload.notification.click_action, '_blank');
                notification.close();
            }
        }

    });
    messaging.onTokenRefresh(function() {
        messaging.getToken()
            .then(function(newtoken) {
                console.log("New Token : " + newtoken);
            })
            .catch(function(reason) {
                console.log(reason);
            })
    })
    IntitalizeFireBaseMessaging();


    function addNewUserToken(token) {
        var _url = "<?= base_url() ?>";
        if (token != '') {

            $.ajax({

                url: _url + 'restaurant/home/token',
                type: 'post',
                data: {
                    'tokenId': token
                },
                dataType: 'json',

                success: function(data) {
                    if (data != '') {

                        console.log('token created');
                    } else {
                        console.log('Failed to add token');
                    }
                }
            });
        }
    }

    function playaudio() {
        // var x = document.getElementById("myAudio");
        // var audio = Audio(x);

        // x.load();
        // x.play();

        var audio = new Audio('https://app.foodoyes.com/dev/assets/orderaudio/femalevoice.mp3');
        audio.preload = 'auto';
        audio.play();
    }
</script>