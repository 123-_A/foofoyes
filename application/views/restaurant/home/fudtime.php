<style>
    .recent_order_left {

        border-right: none !important;
    }
</style>


<div class="space"></div>
<div class="recent_order">
    <div class="recent_order_left">
        <h1>food time</h1>
    </div>
    <!-- <div class="recent_order_right2">
        <div class="recent_order_right2_left"><label>restaurant status</label></div>
        <div class="recent_order_right2_right">
            <label class="switch2">
                <input type="checkbox" checked>
                <span class="slider round"></span>
            </label>
        </div>
        <div class="clear"></div>
    </div> -->
    <div class="clear"></div>
</div>



<div class="list-wrapper">

    <div class="list-item">
        <div class="list_order_inner">
            <ul>
                <?php if ($listresults) : foreach ($listresults as $key => $cats) { ?>
                        <li>
                            <div class="food_time">
                                <h1><?= $cats->food_group_name; ?> time</h1>
                                <div class="food_time_in">
                                    <label>starting time<span>*</span></label>
                                    <input class="st_time_<?= $cats->foodgroupId ?>" name="txtName" value="<?= isset($cats->catTimings->starting_time) ? $cats->catTimings->starting_time : ''; ?>" type="time" id="#" placeholder="Enter Item Name" required />
                                    <span id="#1" class="spn_Error" style="display:none;"></span>
                                </div>

                                <div class="food_time_in">
                                    <label>ending time <span>*</span></label>
                                    <input class="end_time_<?= $cats->foodgroupId ?>" name="txtName" value="<?= isset($cats->catTimings->ending_time) ? $cats->catTimings->ending_time : ''; ?>" type="time" id="#4" placeholder="Enter Item Name" required />
                                    <span id="#3" class="spn_Error" style="display:none;"></span>
                                </div>

                                <div class="food_time_in2">
                                    <input data-foodgroupid="<?= isset($cats->foodgroupId) ? $cats->foodgroupId : ''; ?>" class="submit_btn newtimingadd" name="txtName" type="submit" id="#5" required />
                                    <span id="#6" class="spn_Error" style="display:none;"></span>
                                </div>


                            </div>
                        </li>
                <?php }
                endif; ?>

            </ul>
        </div>
    </div>

</div>
<div id="pagination-container"></div>
</div>

<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>


<script>
    $('.newtimingadd').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _fudctaid = $(this).data('foodgroupid');
        var _st_time = $('.st_time_' + _fudctaid).val();
        var _end_time = $('.end_time_' + _fudctaid).val();
        $.ajax({

            url: _url + 'restaurant/home/newtiming',
            type: 'post',
            data: {
                'catid': _fudctaid,
                '_st_time': _st_time,
                '_end_time': _end_time,
            },
            dataType: 'json',
            // enctype: 'multipart/form-data',

            success: function(data) {
                if (data != '') {
                    alert(data);
                    location.reload(true);

                } else {
                    alert(data);
                }
            }
        });
    });
</script>