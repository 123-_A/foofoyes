<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button .active {
        background-color: #FFFFFF;
    }

    .active {
        color: #FFF !important;
        background-color: #f15733 !important;
        border-color: #000 solid 1px !important;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 17px;
        padding-right: 17px;
        border-radius: 6px;
    }


    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination ul {
        margin: 0 0 20px;
        padding: 0;
        list-style: none;
        text-align: center;
    }

    .pagination li {
        display: inline-block;
        margin-right: 5px;
    }

    .pagination li a,
    .pagination li span {
        color: #ffffff;
        padding: 14px 20px;
        font-family: 'Oswald', sans-serif;
        font-weight: bold;
        text-decoration: none;
        border-radius: 6px;
        background-color: #f15733;
        border-color: #000;
    }

    .pagination li a .active {
        background-color: #FFF !important;
        border-color: #000 solid 1px !important;
    }

    .pagination li a .current {
        background-color: #FFF !important;
        border-color: #000 solid 1px !important;
    }
</style>


<div class="quote">

    <div class="popup_main">
        <div class="obscure">
            <div class="popup animationClose">

                <div class="contact_form">
                    <form action="mailsend.php" method="post" id="foodform">

                        <div class="items_display">
                            <div class="items_display_firstline">
                                <ul>
                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>item name <span>*</span></label>
                                            <input name="itemName" class="itemName_cls" type="text" id="#" placeholder="Enter Item Name" required />
                                            <span id="#1" class="spn_Error" style="display:none;"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>Discription <span>*</span></label>
                                            <input name="description" class="description_cls" type="text" id="#2" placeholder="Enter Discription" required />
                                            <span id="#3" class="spn_Error" style="display:none;"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>category <span>*</span></label>
                                            <select name="fudcats" id="#123" class="items_display_firstline_dropdown fudcats_cls">
                                                <option value="">Select Category</option>
                                                <?php if ($listresults['food_category']) {
                                                    foreach ($listresults['food_category'] as $key => $cats) {
                                                ?>
                                                        <option value="<?= $cats->food_category_id ?>"><?= $cats->food_category_name ?></option>
                                                <?php }
                                                } ?>

                                            </select>
                                        </div>

                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>Tax <span>*</span></label>
                                            <select name="fudtax" id="#123" class="items_display_firstline_dropdown fudtax_cls">
                                                <option value="">Select Category</option>
                                                <?php if ($listresults['tax']) {
                                                    foreach ($listresults['tax'] as $key => $cats) {
                                                ?>
                                                        <option value="<?= $cats->tax_amt ?>"><?= $cats->tax_amt ?></option>
                                                <?php }
                                                } ?>

                                            </select>

                                        </div>
                                    </li>

                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>Type <span>*</span></label>
                                            <select name="fud_types" id="#121" class="items_display_firstline_dropdown fud_types_cls">
                                                <option value="" selected>Select Type</option>
                                                <option value="veg">Veg</option>
                                                <option value="non-veg">Non-Veg</option>

                                            </select>

                                        </div>
                                    </li>


                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>Group <span>*</span></label>
                                            <select name="fudgrp" id="#123" class="items_display_firstline_dropdown fudgrp_cls">
                                                <option value="">Select Category</option>
                                                <?php if ($listresults['food_groups']) {
                                                    foreach ($listresults['food_groups'] as $key => $cats) {
                                                ?>
                                                        <option value="<?= $cats->foodgroupId ?>"><?= $cats->food_group_name ?></option>
                                                <?php }
                                                } ?>

                                            </select>


                                        </div>
                                    </li>

                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>image <span>*</span></label>
                                            <input type="file" class="myfiles_cls" name="newfoodimage" required />
                                            <span id="#12" class="spn_Error" style="display:none;"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>thumb <span>*</span></label>
                                            <input type="file" id="myFile2" class="thumpfile_cls" name="thumpfile" required />
                                            <span id="#13" class="spn_Error" style="display:none;"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="items_display_firstline_inn">
                                            <label>custom message <span>*</span></label>
                                            <label class="switch">
                                                <input type="checkbox" name="chngecusmsg" value="0" class="chngecusmsg_cls">
                                                <!-- checked -->
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </li>


                                </ul>


                                <div class="clear"></div>
                            </div>


                            <div class="add_food_btn">
                                <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                <div class="add_food_btn_right"><a href="#"><i class="fas fa-times"></i></a></div>
                                <div class="clear"></div>
                            </div>
                        </div>





                    </form>
                </div>

                <a class="closeBtn" href="#"></a>
            </div>
        </div>
        <a class="openBtn" href="#">add a new food</a>
    </div>
</div>

<div class="space3"></div>
<form method="post" action="<?= base_url() ?>restaurant/home/ourMenuSearch">
    <div class="recent_order">
        <div class="recent_order_left">
            <h1>recent orders</h1>
        </div>
        <div class="recent_order_right">
            <div class="recent_order_right_left">
                <input type="text" name="fudname" class="search-place srchhere" placeholder="Search">
            </div>

            <div class="recent_order_right_right"><button type="submit"><img src="<?= base_url() ?>assets/search.png" alt="" /></button></div>

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</form>


<div class="list-wrapper">
    <?php if ($menuitems) {
        foreach ($menuitems as $Mkeys => $items) : ?>
            <div class="list-item">
                <div class="our_menu_head">
                    <div class="our_menu_head_left">
                        <h1>food category</h1>
                    </div>
                    <div class="our_menu_head_right">
                        <h1><?= $items->category; ?></h1>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="our_menu_data">


                    <table class="dataTable" id="">
                        <thead>
                            <tr>
                                <th>item name</th>

                                <th>status</th>
                                <th>Manage food status</th>
                                <th> actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($menuitems[$Mkeys]->foodscat) {
                                foreach ($menuitems[$Mkeys]->foodscat as $ks => $vls) { ?>
                                    <tr>
                                        <td style="text-align: left;"><?= $vls->item_name; ?> </td>

                                        <td><?php if ($vls->restaurant_status == 0) { ?> Inactive <?php } else { ?> Active <?php } ?></td>
                                        <td>
                                            <div class="data_butn">
                                                <ul>
                                                    <li>

                                                        <?php if ($vls->restaurant_status == 0) { ?>


                                                            <div class="active_btn changefudstsz" data-allmainid="<?= $vls->master_id; ?>"><a href="#">make active</a></div>

                                                        <?php } else { ?> <div class="inactive_btn changefudstsz" data-allmainid="<?= $vls->master_id; ?>"><a href="#">make inactive</a></div><?php } ?>
                                                    </li>
                                                    <!-- <li>
                                                <div class="active_btn"><a href="#">active offer</a></div>

                                            </li> -->
                                                </ul>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="data_butn">
                                                <ul>
                                                    <!-- <li>
                                                        <div class="view_btn"><a href="#">view</a></div>
                                                    </li> -->
                                                    <li>
                                                        <div class="edit_btn"><a href="<?= base_url() ?>restaurant/edit/<?= base64_encode($vls->master_id) ?>"><i class="fas fa-pencil-alt"></i></a></div>
                                                    </li>
                                                    <li>
                                                        <div class="edit_btn "><a class="deleteorder" data-deleteidd="<?= $vls->master_id; ?>" href="#"><i class="fas fa-trash-alt "></i></a></div>
                                                    </li>
                                                </ul>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                            <?php }
                            }  ?>

                        </tbody>
                    </table>

                </div>

            </div>
        <?php endforeach; ?>
        <div id="pagination-container">
        <?php echo $this->pagination->create_links();
    } ?>
        </div>



</div>




<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js"></script>
<script src="js/pagination.js"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>

<script>
    $('.chngecusmsg_cls').click(function() {
        var _sts = $(this).val();
        if (_sts == 0) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });


    $('.add_food_btn_left').click(function() {

        var formData = new FormData($('#foodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/newfud',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('Food added');
                    location.reload(true);

                } else {
                    alert('Failed to add food');
                }
            }
        });
    });
</script>

<script>
    $('.changefudstsz').click(function(e) {
        e.preventDefault();
        var _stsval = $(this).attr('data-allmainid');
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/changerestfusstatus',
            type: 'post',
            data: {
                'restaurant_id': _stsval
            },
            dataType: 'json',
            // enctype: 'multipart/form-data',

            success: function(data) {
                if (data != '') {
                    alert(data);
                    location.reload(true);

                } else {
                    alert(data);
                }
            }
        });
    });
</script>


<script>
    $('.deleteorder').click(function() {
        if (confirm("Are you sure you want to delete this?")) {
            var _deleteid = $(this).data('deleteidd');
            var _url = "<?= base_url() ?>";
            $.ajax({

                url: _url + 'restaurant/home/deletefoodmaster',
                type: 'post',
                data: {
                    'deleteid': _deleteid
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert(data);
                        location.reload(true);

                    } else {
                        alert(data);
                    }
                }
            });
        }
    });
</script>