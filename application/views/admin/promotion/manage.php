<style>
    /* pop up style */

    .cd-nugget-info {
        text-align: center;
        position: absolute;
        width: 100%;
        height: 50px;
        line-height: 50px;
        bottom: 0;
        left: 0;
    }

    .cd-nugget-info a {
        position: relative;
        font-size: 14px;
        color: #5e6e8d;
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
        transition: all 0.2s;
    }

    .no-touch .cd-nugget-info a:hover {
        opacity: .8;
    }

    .cd-nugget-info span {
        vertical-align: middle;
        display: inline-block;
    }

    .cd-nugget-info span svg {
        display: block;
    }

    .cd-nugget-info .cd-nugget-info-arrow {
        fill: #5e6e8d;
    }

    .cd-popup-trigger {
        display: block;
        line-height: 50px;
        margin: 0 auto;
        padding: 10px 10px;
        text-align: center;
        color: #FFF;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        border-radius: 10%;
        background: #f15733;
        box-shadow: 0 3px 0 rgba(0, 0, 0, 0.07);
    }

    .cd-popup-trigger .fa {
        font-size: 15px;

    }




    @media only screen and (min-width: 1170px) {
        .cd-popup-trigger {
            /* margin: 6em auto; */
            margin: 1% auto;
        }
    }

    /* -------------------------------- 

xpopup 

-------------------------------- */
    .cd-popup {
        position: fixed;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(94, 110, 141, 0.9);
        opacity: 0;
        visibility: hidden;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        transition: opacity 0.3s 0s, visibility 0s 0.3s;
    }

    .cd-popup.is-visible {
        opacity: 1;
        visibility: visible;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0s;
        transition: opacity 0.3s 0s, visibility 0s 0s;
    }

    .cd-popup-container {
        position: relative;
        width: 75%;
        margin: 4em auto;
        background: #FFF;
        border-radius: .25em .25em .4em .4em;
        text-align: center;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
        -webkit-transform: translateY(-40px);
        -moz-transform: translateY(-40px);
        -ms-transform: translateY(-40px);
        -o-transform: translateY(-40px);
        transform: translateY(-40px);
        /* Force Hardware Acceleration in WebKit */
        -webkit-backface-visibility: hidden;
        -webkit-transition-property: -webkit-transform;
        -moz-transition-property: -moz-transform;
        transition-property: transform;
        -webkit-transition-duration: 0.3s;
        -moz-transition-duration: 0.3s;
        transition-duration: 0.3s;
    }

    .cd-popup-container p {
        padding: 3em 1em;
    }

    .cd-popup-container .cd-buttons:after {
        content: "";
        display: table;
        clear: both;
    }

    .cd-popup-container .cd-buttons li {
        float: left;
        width: 50%;
        list-style: none;
    }

    .cd-popup-container .cd-buttons a {
        display: block;
        height: 60px;
        line-height: 60px;
        text-transform: uppercase;
        color: #FFF;
        -webkit-transition: background-color 0.2s;
        -moz-transition: background-color 0.2s;
        transition: background-color 0.2s;
    }

    .cd-popup-container .cd-buttons li:first-child a {
        background: #fc7169;
        border-radius: 0 0 0 .25em;
    }

    .no-touch .cd-popup-container .cd-buttons li:first-child a:hover {
        background-color: #fc8982;
    }

    .cd-popup-container .cd-buttons li:last-child a {
        background: #b6bece;
        border-radius: 0 0 .25em 0;
    }

    .no-touch .cd-popup-container .cd-buttons li:last-child a:hover {
        background-color: #c5ccd8;
    }

    .cd-popup-container .cd-popup-close {
        position: absolute;
        top: 8px;
        right: 8px;
        width: 30px;
        height: 30px;
    }

    .cd-popup-container .cd-popup-close::before,
    .cd-popup-container .cd-popup-close::after {
        content: '';
        position: absolute;
        top: 12px;
        width: 14px;
        height: 3px;
        background-color: #8f9cb5;
    }

    .cd-popup-container .cd-popup-close::before {
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        left: 8px;
    }

    .cd-popup-container .cd-popup-close::after {
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
        right: 8px;
    }

    .is-visible .cd-popup-container {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        -o-transform: translateY(0);
        transform: translateY(0);
    }

    @media only screen and (min-width: 1170px) {
        .cd-popup-container {
            margin: 8em auto;
        }
    }



    /* pop up style end */



    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>

<div class="quote">

    <div class="popup_main">
        <div class="obscure">
            <div class="popup2 animationClose">

                <div class="contact_form">
                    <form action="mailsend.php" method="post" id="promotionform">

                        <div class="items_display">
                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>Country<span>*</span></label>
                                        <select id="#123" name="countryId" class="items_display_firstline2_dropdown country_name">

                                            <?php if ($country) {
                                            ?> <option value="" selected>Select Country</option> <?php
                                                                                                    foreach ($country as $key => $cntry) { ?>
                                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                                <?php }
                                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                                        </select>
                                    </li>
                                    <li><label>State<span>*</span></label>
                                        <select name="stateid" id="#123" class="items_display_firstline2_dropdown state_name">
                                            <option value="">Select State</option>
                                        </select>
                                    </li>
                                    <li><label>District<span>*</span></label>
                                        <select id="#123" name="districtId" class="items_display_firstline2_dropdown district_name">
                                            <option value="">District State</option>

                                        </select>
                                    </li>


                                </ul>


                                <div class="clear"></div>
                            </div>


                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>Restaurant<span>*</span></label>
                                        <select name="cityId" id="#123" class="items_display_firstline2_dropdown city_name">
                                            <option value="" selected>Select Restaurant</option>

                                        </select>
                                    </li>


                                    <li><label>Promotion Banner <span>*</span></label>
                                        <input type="file" id="myFile2" name="bannerimage[]" multiple="multiple" required />
                                        <span id="#13" class="spn_Error" style="display:none;"></span>
                                    </li>





                                </ul>


                                <div class="clear"></div>
                            </div>



                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>Starting Date<span>*</span></label>
                                        <input name="stdate" type="date" id="#8" placeholder="Joining Date" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>


                                    <li><label>Ending Date<span>*</span></label>
                                        <input name="enddate" type="date" id="#8" placeholder="Joining Date" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>





                                </ul>


                                <div class="clear"></div>
                            </div>



                            <div class="add_food_btn">
                                <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                <div class="add_food_btn_right"><a href="#"><i class="fas fa-redo-alt"></i></a></div>
                                <div class="clear"></div>
                            </div>
                        </div>





                    </form>
                </div>

                <a class="closeBtn" href="#"></a>
            </div>
        </div>
        <a class="openBtn" href="#">Add a promotion</a>
    </div>

</div>


<div class="space">
    <div class="varient_item2">

        <div class="items_display_firstline">
            <ul>

                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>Country</option>
                            <option value="gst0">India</option>
                            <option value="gst18">india</option>

                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>state</option>
                            <option value="gst0">state 1</option>
                            <option value="gst18">state 2</option>

                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>district</option>
                            <option value="gst0">district 1</option>
                            <option value="gst5">district 2</option>

                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>city</option>
                            <option value="gst0">city 1</option>
                            <option value="gst5">city 2</option>
                            <option value="gst12">city 3</option>


                        </select>
                    </div>
                </li>


                <li>
                    <div class="menu_edit_btn_new">
                        <ul>
                            <li>

                                <input class="submit_btn" name="txtName" type="submit" id="#34" required />
                                <span id="#35" class="spn_Error" style="display:none;"></span>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </li>


            </ul>


            <div class="clear"></div>
        </div>



        <div class="clear"></div>
    </div>
    <div class="recent_order">

        <a href="#">
            <div class="recent_order_left2">
                <h1>promotion List</h1>
            </div>
        </a>



        <div class="clear"></div>


    </div>


    <div class="list-wrapper">
        <div class="list-item">

            <div class="our_menu_data">


                <table class="dataTable" id="example31">
                    <thead>
                        <tr>
                            <th>SL no</th>

                            <th> Country</th>
                            <th> State</th>
                            <th> District</th>
                            <th> Pro Start date</th>
                            <th> Pro Ending date</th>

                            <th> Active/Inactive</th>
                            <th> Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($promotions) {
                            $i = 1;
                            foreach ($promotions as $key => $cats) { ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><?= $cats->country_name; ?></td>
                                    <td><?= $cats->state_name; ?></td>
                                    <td><?= $cats->district_name; ?></td>
                                    <td><?= $cats->starting_date; ?></td>
                                    <td><?= $cats->ending_date; ?></td>
                                    <td>
                                        <div class="data_butn2">
                                            <ul>
                                                <li>
                                                    <div class="data_butn2_left">
                                                        <p>off</p>
                                                    </div>
                                                    <label class="switch3">
                                                        <input type="checkbox" class="btn_actv" data-actval="<?= $cats->ad_promotion_id; ?>" <?php if ($cats->is_active == 1) { ?> checked <?php } ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <div class="data_butn2_right">
                                                        <p>on</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="data_butn">
                                            <ul>
                                                <!-- <li>
                                                    <div class="edit_btn"><a href="#"><i class="fas fa-pencil-alt"></i></a></div>
                                                </li> -->
                                                <li>
                                                    <div class="edit_btn"><a class="deletepromo" data-promoid="<?= $cats->ad_promotion_id; ?>" href="#"><i class="fas fa-trash-alt"></i></a></div>
                                                </li>
                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>




                </table>

            </div>

        </div>






    </div>



</div>
<div class="list-wrapper">
    <div class="data_butn">
        <ul>
            <li>
                <a href="#0" class="cd-popup-trigger"><i class="fas fa-eye"></i></a>

                <div class="cd-popup cd-popup" role="alert">
                    <div class="cd-popup-container">
                        <div class="contact_form">
                            <form action="mailsend.php" method="post" id="promotionformzzzz">

                                <div class="items_display">
                                    <div class="items_display2_firstline">
                                        <ul>
                                            <li><label>Country<span>*</span></label>
                                                <select id="#123" name="countryId" class="items_display_firstline2_dropdown country_name">

                                                    <?php if ($country) {
                                                    ?> <option value="" selected>Select Country</option> <?php
                                                                                                            foreach ($country as $key => $cntry) { ?>
                                                            <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                                        <?php }
                                                                                                        } else { ?> <option value="">Country not found</option> <?php } ?>




                                                </select>
                                            </li>
                                            <li><label>State<span>*</span></label>
                                                <select name="stateid" id="#123" class="items_display_firstline2_dropdown state_name">
                                                    <option value="">Select State</option>
                                                </select>
                                            </li>
                                            <li><label>District<span>*</span></label>
                                                <select id="#123" name="districtId" class="items_display_firstline2_dropdown district_name">
                                                    <option value="">District State</option>

                                                </select>
                                            </li>


                                        </ul>


                                        <div class="clear"></div>
                                    </div>


                                    <div class="items_display2_firstline">
                                        <ul>
                                            <li><label>City<span>*</span></label>
                                                <select name="cityId" id="#123" class="items_display_firstline2_dropdown city_name">
                                                    <option value="" selected>Select City</option>

                                                </select>
                                            </li>


                                            <li><label>Promotion Banner <span>*</span></label>
                                                <input type="file" id="myFile2" name="bannerimage[]" multiple="multiple" required />
                                                <span id="#13" class="spn_Error" style="display:none;"></span>
                                            </li>





                                        </ul>


                                        <div class="clear"></div>
                                    </div>



                                    <div class="items_display2_firstline">
                                        <ul>
                                            <li><label>Starting Date<span>*</span></label>
                                                <input name="stdate" type="date" id="#8" placeholder="Joining Date" required />
                                                <span id="#9" class="spn_Error" style="display:none;"></span>
                                            </li>


                                            <li><label>Ending Date<span>*</span></label>
                                                <input name="enddate" type="date" id="#8" placeholder="Joining Date" required />
                                                <span id="#9" class="spn_Error" style="display:none;"></span>
                                            </li>





                                        </ul>


                                        <div class="clear"></div>
                                    </div>



                                    <div class="add_food_btn">
                                        <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                        <div class="add_food_btn_right"><a href="#"><i class="fas fa-redo-alt"></i></a></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>





                            </form>
                        </div>

                    </div>
                    <!-- cd-popup-container -->
                </div> <!-- cd-popup -->
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</div>



<div id="pagination-container"></div>

</div>

<!--OWL SECTION ENDS HERE-->

<div class="clear"></div>
<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function($) {
        //open popup
        var _url = "<?= base_url() ?>";
        $('.cd-popup-trigger').on('click', function(event) {
            event.preventDefault();

            $('.cd-popup').addClass('is-visible');
        });

        //close popup
        $('.cd-popup').on('click', function(event) {
            if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                event.preventDefault();
                $(this).removeClass('is-visible');
            }
        });
        //close popup when clicking the esc keyboard button
        $(document).keyup(function(event) {
            if (event.which == '27') {
                $('.cd-popup').removeClass('is-visible');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>

<script>
    $(function() {
        $('.country_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cntryId = $(this).val();
            if (_cntryId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/adpromotion/getstatebyid',
                    data: {
                        'countryid': _cntryId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value="">Select State</option>';
                            html += '<option value="all">All</option>';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.state_id + '">' + val.state_name + '</option>';
                            });
                            $('.state_name').html(html);
                            $('.city_name').html('<option value="">Select City</option>');
                            $('.district_name').html('<option value="">Select District</option>');

                        } else {
                            alert('no state found');
                        }
                    }
                });
            }
        });
        // get district by state changes
        $('.state_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cityId = $(this).val();

            if ((_cityId != '') && (_cityId != 'all')) {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/adpromotion/getdistrictbyid',
                    data: {
                        'cityid': _cityId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value="">Select District</option>';
                            html += '<option value="all">All</option>';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.district_id + '">' + val.district_name + '</option>';
                            });
                            $('.district_name').html(html);
                            $('.city_name').html('<option value="">Select City</option>');

                        } else {
                            alert('no states found');
                        }
                    }
                });
            } else if (_cityId == 'all') {
                var html = '';
                var _cntry = $('.country_name').val();

                html += '<option value="all">All</option>';
                $('.district_name').html(html);
                // restaurants list

                $.ajax({
                    method: 'post',
                    url: _url + 'admin/adpromotion/getdistrictsrestaurants',
                    data: {
                        'cityid': _cntry
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {

                            var html = '';
                            html += '<option value="">Select Restaurant</option>';

                            $.each(data, function(key, val) {
                                html += '<option value="' + val.restaurant_id + '">' + val.restaurant_name + '</option>';
                            });
                            $('.city_name').html(html);

                        } else {
                            alert('no restaurants found');
                        }
                    }
                });
            }
        });
        // get cities by district id
        $('.district_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cityId = $(this).val();
            if ((_cityId != '') && (_cityId != 'all')) {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/adpromotion/getdisres',
                    data: {
                        'cityid': _cityId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {

                            var html = '';
                            html += '<option value="">Select Restaurant</option>';

                            $.each(data, function(key, val) {
                                html += '<option value="' + val.restaurant_id + '">' + val.restaurant_name + '</option>';
                            });
                            $('.city_name').html(html);

                        } else {
                            alert('no restaurants found');
                        }
                    }
                });
            } else if (_cityId == 'all') {
                // var html = '';
                // html += '<option value="all">All</option>';

                var _cntry = $('.state_name').val();
                // $('.city_name').html(html);


                $.ajax({
                    method: 'post',
                    url: _url + 'admin/adpromotion/getdistrictsrestaurants',
                    data: {
                        'cityid': _cntry
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {

                            var html = '';
                            html += '<option value="">Select Restaurant</option>';

                            $.each(data, function(key, val) {
                                html += '<option value="' + val.restaurant_id + '">' + val.restaurant_name + '</option>';
                            });
                            $('.city_name').html(html);

                        } else {
                            alert('no restaurants found');
                        }
                    }
                });
            }
        });
    });

    // form submit 
    $('.add_food_btn_left').click(function() {
        var formData = new FormData($('#promotionform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'admin/adpromotion/newpromotion',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('Promotion  added');
                    $('.closeBtn').click();
                    location.reload(true);
                } else {
                    alert('Failed to add Promotion');
                }
            }
        });
    });
</script>

<!-- //active inactiv -->
<script>
    $('.btn_actv').click(function() {
        var _id = $(this).data('actval');
        var _url = "<?= base_url() ?>";

        $.ajax({
            method: 'post',
            url: _url + 'admin/adpromotion/changereststs',
            data: {
                'id': _id,

            },
            success: function(data) {
                alert('Promotion  Status changed Successfully');
                // $('.closeBtn').click();
                location.reload(true);
            },
            error: function(data) {
                alert('Failed to changed status');
                $('.add_food_btn_left').click('readonly', false);
            },
        });
    });

    $('.deletepromo').click(function(e) {
        e.preventDefault();
        if (confirm('Delete this promotion')) {
            var _id = $(this).data('promoid');
            var _url = "<?= base_url() ?>";

            $.ajax({
                method: 'post',
                url: _url + 'admin/adpromotion/deletepromo',
                data: {
                    'id': _id,

                },
                success: function(data) {
                    alert('Promotion  deleted Successfully');
                    // $('.closeBtn').click();
                    location.reload(true);
                },
                error: function(data) {
                    alert('Failed to delete promotion');
                    // $('.add_food_btn_left').click('readonly', false);
                },
            });
        }
    });
</script>