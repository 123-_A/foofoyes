<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <title>Foodoyes Dashboard</title>
    <link rel="shortcut icon" type="<?= base_url() ?>resources/image/png" href="<?= base_url() ?>resources/images/favicon.png" />
    <link href="<?= base_url() ?>resources/css/foodoyes.css" rel="stylesheet" type="text/css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="<?= base_url() ?>resources/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>resources/css/owl.css" rel="stylesheet" type="text/css" />
    <meta name="<?= base_url() ?>resources/description" content="UnniMaash">
    <meta name="keywords" content="UnniMaash">
    <link href="<?= base_url() ?>resources/fonts/fontawesome-free-5.12.0-web/css/all.css" rel="stylesheet" />

</head>

<body>

    <!--INDEX SLIDER-->




    <div class="login_bagckground">


        <div class="foobi"><img src="<?= base_url() ?>resources/images/foobi.png" alt="FOODOYES APPS" /></div>

        <form id="loginform" method="post" action="<?= base_url() ?>admin/login/logincheck">
            <div class="login_content">
                <div class="login_fild">
                    <div class="items_display2">
                        <div class="items_display2_firstline_login">
                            <ul>
                                <li>
                                    <?php

                                    if ($this->session->flashdata('danger')) {
                                        echo  '<center><h2 style="color:red">' . $message = $this->session->flashdata('danger') . '</h2></center>';
                                    }
                                    ?>
                                    <div class="items_display2_firstline_inn_login">
                                        <label>name<span>*</span></label>
                                        <input name="partnername" type="text" id="#" placeholder="Partner Name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                        <?php if (form_error('partnername')) {
                                            echo "<span style='color:red'>" . form_error('partnername') . "</span>";
                                        }
                                        ?>
                                    </div>
                                </li>

                                <li>
                                    <div class="items_display2_firstline_inn_login">
                                        <label>Password<span>*</span></label>
                                        <input name="password" type="password" id="#" placeholder="Enter Password" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                        <?php if (form_error('password')) {
                                            echo "<span style='color:red'>" . form_error('password') . "</span>";
                                        }
                                        ?>
                                    </div>
                                </li>

                                <li>

                                    <button type="submit" class="openBtn_login btn-lg" style="width: 200px  !important;">Login Now</button>


                            </ul>


                            <div class="clear"></div>
                        </div>


                    </div>
                </div>

            </div>
        </form>
    </div>





    <!--OWL SECTION ENDS HERE-->


    <script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>

</body>

</html>