<style>
    /* pop up style */

    .cd-nugget-info {
        text-align: center;
        position: absolute;
        width: 100%;
        height: 50px;
        line-height: 50px;
        bottom: 0;
        left: 0;
    }

    .cd-nugget-info a {
        position: relative;
        font-size: 14px;
        color: #5e6e8d;
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
        transition: all 0.2s;
    }

    .no-touch .cd-nugget-info a:hover {
        opacity: .8;
    }

    .cd-nugget-info span {
        vertical-align: middle;
        display: inline-block;
    }

    .cd-nugget-info span svg {
        display: block;
    }

    .cd-nugget-info .cd-nugget-info-arrow {
        fill: #5e6e8d;
    }

    .cd-popup-trigger {
        display: block;
        line-height: 50px;
        margin: 0 auto;
        padding: 10px 10px;
        text-align: center;
        color: #FFF;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        border-radius: 10%;
        background: #f15733;
        box-shadow: 0 3px 0 rgba(0, 0, 0, 0.07);
    }

    .cd-popup-trigger .fa {
        font-size: 15px;

    }




    @media only screen and (min-width: 1170px) {
        .cd-popup-trigger {
            /* margin: 6em auto; */
            margin: 1% auto;
        }
    }

    /* -------------------------------- 

xpopup 

-------------------------------- */
    .cd-popup {
        position: fixed;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(94, 110, 141, 0.9);
        opacity: 0;
        visibility: hidden;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        transition: opacity 0.3s 0s, visibility 0s 0.3s;
    }

    .cd-popup.is-visible {
        opacity: 1;
        visibility: visible;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0s;
        transition: opacity 0.3s 0s, visibility 0s 0s;
    }

    .cd-popup-container {
        position: relative;
        width: 90%;
        max-width: 400px;
        margin: 4em auto;
        background: #FFF;
        border-radius: .25em .25em .4em .4em;
        text-align: center;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
        -webkit-transform: translateY(-40px);
        -moz-transform: translateY(-40px);
        -ms-transform: translateY(-40px);
        -o-transform: translateY(-40px);
        transform: translateY(-40px);
        /* Force Hardware Acceleration in WebKit */
        -webkit-backface-visibility: hidden;
        -webkit-transition-property: -webkit-transform;
        -moz-transition-property: -moz-transform;
        transition-property: transform;
        -webkit-transition-duration: 0.3s;
        -moz-transition-duration: 0.3s;
        transition-duration: 0.3s;
    }

    .cd-popup-container p {
        padding: 3em 1em;
    }

    .cd-popup-container .cd-buttons:after {
        content: "";
        display: table;
        clear: both;
    }

    .cd-popup-container .cd-buttons li {
        float: left;
        width: 50%;
        list-style: none;
    }

    .cd-popup-container .cd-buttons a {
        display: block;
        height: 60px;
        line-height: 60px;
        text-transform: uppercase;
        color: #FFF;
        -webkit-transition: background-color 0.2s;
        -moz-transition: background-color 0.2s;
        transition: background-color 0.2s;
    }

    .cd-popup-container .cd-buttons li:first-child a {
        background: #fc7169;
        border-radius: 0 0 0 .25em;
    }

    .no-touch .cd-popup-container .cd-buttons li:first-child a:hover {
        background-color: #fc8982;
    }

    .cd-popup-container .cd-buttons li:last-child a {
        background: #b6bece;
        border-radius: 0 0 .25em 0;
    }

    .no-touch .cd-popup-container .cd-buttons li:last-child a:hover {
        background-color: #c5ccd8;
    }

    .cd-popup-container .cd-popup-close {
        position: absolute;
        top: 8px;
        right: 8px;
        width: 30px;
        height: 30px;
    }

    .cd-popup-container .cd-popup-close::before,
    .cd-popup-container .cd-popup-close::after {
        content: '';
        position: absolute;
        top: 12px;
        width: 14px;
        height: 3px;
        background-color: #8f9cb5;
    }

    .cd-popup-container .cd-popup-close::before {
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        left: 8px;
    }

    .cd-popup-container .cd-popup-close::after {
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
        right: 8px;
    }

    .is-visible .cd-popup-container {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        -o-transform: translateY(0);
        transform: translateY(0);
    }

    @media only screen and (min-width: 1170px) {
        .cd-popup-container {
            margin: 8em auto;
        }
    }





    /* pop up style end */



    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>

<div class="recent_order">
    <a href="#">
        <div class="recent_order_left2">
            <h1>recent post</h1>
        </div>
    </a>

    <div class="clear"></div>
</div>





<div class="list-wrapper">

    <div class="list-item">

        <div class="our_menu_data">


            <table class="" id="example3">
                <thead>
                    <tr>
                        <th>SL NO</th>
                        <th>User name</th>
                        <th>Post Title</th>
                        <th>Post Type</th>
                        <th> View</th>
                        <th>Approve / reject</th>
                        <th>Trash</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($ufiles) {
                        $i = 1;
                        foreach ($ufiles as $key => $res) { ?>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td> <?= $res->first_name; ?></td>
                                <td> <?= $res->post_title; ?></td>
                                <td> <?php if ($res->file_type == 1) {
                                            echo 'Video';
                                        } else {
                                            echo 'Image';
                                        } ?></td>
                                <td>

                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <a href="#0" data-unqid="<?= $res->filesid; ?>" data-posttype="<?= $res->file_type; ?>" data-cvideolink="<?= $res->file; ?>" class="cd-popup-trigger"><i class="fas fa-eye"></i></a>

                                                <div class="cd-popup cd-popup_<?= $res->filesid; ?>" role="alert">
                                                    <div class="cd-popup-container">
                                                        <div class="myVideo_<?= $res->filesid; ?>">
                                                        </div>
                                                        <!-- <video class="myVideo_<?= $res->filesid; ?>" width="320" height="176">



                                                            Your browser does not support HTML5 video.
                                                        </video>
                                                        <button onclick="playVid(<?= $res->filesid; ?>)" type="button">Play Video</button>
                                                        <button onclick="pauseVid(<?= $res->filesid; ?>)" type="button">Pause Video</button><br>

                                                        <a href="#0" class="cd-popup-close img-replace">Close</a> -->
                                                    </div>
                                                    <!-- cd-popup-container -->
                                                </div> <!-- cd-popup -->
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                                <td>
                                    <?php if ($res->is_active == 0) { ?>
                                        <div class="list_order_bottom_right_left"><a href="#" class="aprv_post" data-postid="<?= $res->filesid; ?>"><i class="fas fa-check"></i></a></div>
                                    <?php } else {  ?>
                                        <div class="list_order_bottom_right_right"><a href="#" class="rejct" data-postid="<?= $res->filesid; ?>"><i class="fas fa-times"></i></a></div>
                                    <?php } ?>
                                </td>
                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <div class="edit_btn"><a href="#" class="deletepost" data-postsid=" <?= $res->filesid; ?>"><i class="fas fa-trash-alt"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>




            </table>




        </div>



    </div>







</div>



</div>








<div class="clear"></div>


</div>



<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>



<script script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript">
</script>
<script>
    $(function() {
        $('.edit_btn').click(function(e) {
            setTimeout(function() {
                $('.popup2').removeClass('animationClose').addClass('animationOpen');
            }, 100);
            $('.obscure').fadeIn(50);
            e.preventDefault();
        });
    });
</script>



<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [10]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [10]
            ]
        });


    });
</script>

<script>
    $('.aprv_post').click(function(e) {
        var _id = $(this).data('postid');
        var _url = "<?= base_url() ?>";

        $.ajax({
            method: 'post',
            url: _url + 'admin/recentpost/aprovepost',
            data: {
                'id': _id,

            },
            success: function(data) {
                alert('Post approved  Successfully');
                // $('.closeBtn').click();
                location.reload(true);
            },
            error: function(data) {
                alert('Failed to Approve post');
                $('.add_food_btn_left').click('readonly', false);
            },
        });
    });
    $('.rejct').click(function(e) {
        var _id = $(this).data('postid');
        var _url = "<?= base_url() ?>";

        $.ajax({
            method: 'post',
            url: _url + 'admin/recentpost/aprovepost',
            data: {
                'id': _id,

            },
            success: function(data) {
                alert('Post Rejected   Successfully');
                // $('.closeBtn').click();
                location.reload(true);
            },
            error: function(data) {
                alert('Failed to reject post');
                $('.add_food_btn_left').click('readonly', false);
            },
        });
    });
    // delete post
    $('.deletepost').click(function(e) {
        var _id = $(this).data('postsid');
        var _url = "<?= base_url() ?>";

        $.ajax({
            method: 'post',
            url: _url + 'admin/recentpost/deleteposts',
            data: {
                'id': _id,

            },
            success: function(data) {
                alert('Post deleted   Successfully');
                // $('.closeBtn').click();
                location.reload(true);
            },
            error: function(data) {
                alert('Failed to deleted post');
                $('.add_food_btn_left').click('readonly', false);
            },
        });
    });
</script>

<!-- pop up script  -->


<script>
    // jQuery(document).ready(function($) {
    $(document).ready(function() {
        //open popup
        var _url = "<?= base_url() ?>";
        $(document).on('click', '.cd-popup-trigger', function(event) {
            event.preventDefault();
            var _vlnk = $(this).data('cvideolink');
            var _posttype = $(this).data('posttype');
            var _unqid = $(this).data('unqid');
            // video

            if (_posttype == 1) {
                $(".myVideo_" + _unqid).html('<video width="400" controls><source src="' + _url + _vlnk + '" type="video/mp4"><source src="' + _url + _vlnk + '" type="video/ogg"> Your browser does not support HTML video.</video>');
            } else {
                console.log(_vlnk);
                $(".myVideo_" + _unqid).html('  <img style="height:250px;width:400px;" src="' + _url + _vlnk + '" alt="FOODOYES APPS">');
            }
            $('.cd-popup_' + _unqid).addClass('is-visible');
        });

        //close popup
        $('.cd-popup').on('click', function(event) {
            if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                event.preventDefault();
                $(this).removeClass('is-visible');
            }
        });
        //close popup when clicking the esc keyboard button
        $(document).keyup(function(event) {
            if (event.which == '27') {
                $('.cd-popup').removeClass('is-visible');
            }
        });
    });





    function playVid(_unqida) {
        var vid = $(".myVideo_" + _unqida);
        vid.play();
    }

    function pauseVid(_unqida) {

        var vid = $(".myVideo_" + _unqida);
        vid.pause();
    }
</script>

<!-- pop up script end -->