<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Registration extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        header('Access-Control-Alloe-Origin: *');
        $this->load->model('Login_model');
    }
    public function index()
    {
        $this->load->view('welcome_message');
    }
    public function signup()
    {

        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        $email = isset($_POST['email']) ? trim($_POST['email']) : '';
        if (!$mobileNumber) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobileNumber is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        $this->validation = $this->validateMobileNumber($mobileNumber);

        if (!$this->validation) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'Please enter a valid mobile number'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (array_key_exists('error', $this->validation)) {
            if ($this->validation['error'] == 'invalid mobile') {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => $this->validation['message']];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            } else {

                $message = ['status' => true, 'statusCode' => Created, 'message' => 'otp generated'];
                $this->json_output(Created, array('status' => Created, 'message' =>  $message));
            }
        } else {
            $otp = $this->common->otpGenerator(4);
            $this->result = $this->Login_model->signup($mobileNumber, $otp, $email);
            $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'message' => $this->result->message));
        }
    }
    public function resendOtp()
    {
        $json_obj = $this->readJson();
        $mobile = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        if (!$mobile) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobile is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->result = $this->Login_model->resendOtp($mobile);
            $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'message' => $this->result->message));
        }
    }
    public function varifyOtp()
    {

        $otp = isset($_POST['otp']) ? trim($_POST['otp']) : '';
        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        if (!$otp) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'otp is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$mobileNumber) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobileNumber is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->validation = $this->Login_model->validateOtp($otp, $mobileNumber);
            if ($this->validation->status === true) {
                // $data = $this->$this->validation;
                $this->json_output($this->validation->statusCode, array('status' => $this->validation->statusCode, 'message' => $this->validation->message, 'user_id' => $this->validation->user_id, 'mobile' => $this->validation->mobile, 'is_active' => $this->validation->is_active));
            } else {
                // $data = $this->$this->validation;
                $this->json_output($this->validation->statusCode, array('status' => $this->validation->statusCode, 'message' => $this->validation->message));
            }
        }
    }
    public function updateProfile()
    {
        $json_obj = $this->readJson();

        // image upload

        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            // $error = array('error' => $this->upload->display_errors());
            $this->insert_a['profile_image'] = 'null';
        } else {
            $data = array('image_metadata' => $this->upload->data());
            $iname = 'images/' . $data['image_metadata']['file_name'];
            $this->insert_a['profile_image'] = $iname;
        }
        // end image upload

        $firstName = isset($_POST['firstName']) ? trim($_POST['firstName']) : '';
        $nickName = isset($_POST['nickName']) ? trim($_POST['nickName']) : '';
        $email = isset($_POST['email']) ? trim($_POST['email']) : '';
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $accountType = isset($_POST['accountType']) ? trim($_POST['accountType']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';
        $c_password = isset($_POST['c_password']) ? trim($_POST['c_password']) : '';
        $is_active = isset($_POST['is_active']) ? trim($_POST['is_active']) : '';
        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';


        if ($firstName) {
            $this->insert_a['first_name'] = $firstName;
        } else if (!$firstName) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'firstName is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if ($nickName) {
            $this->insert_a['nickName'] = $nickName;
        }
        $this->insert_a['mobile'] = $mobileNumber;

        if ($email) {
            $this->insert_a['email'] = $email;
        } else if (!$email) {
            $this->validEmail($email);
        }

        if (!$password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'password is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else if (!$c_password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'confirm password is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else if ($password != $c_password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'password mismatch'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->insert_a['password'] = md5($password);
        }

        if (!$accountType) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'account type is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->insert_a['account_type'] = $accountType;
        }

        if (isset($json_obj->mobileNumber)) {
            $this->insert_a['mobile'] = $json_obj->mobileNumber;
        }
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $userId = $userId;
        }
        if ($firstName && $email && $password && $accountType && $userId && $c_password && $password == $c_password) {
            $this->results =   $this->Login_model->checkUserExist($email, $nickName, $mobileNumber);

            if ($this->results->status == true) {
                $this->result = $this->Login_model->updateProfile($this->insert_a, $userId);
                $this->result_new = $this->Login_model->activateAccount($userId, $is_active);
                if ($this->result_new) {
                    $this->json_output($this->result_new->statusCode, array('status' => $this->result_new->statusCode, 'message' => $this->result_new->message, 'user_id' => $userId));
                }
            } else {
                // $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => $this->results->message];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $this->results->message));
            }
        }
    }

    public function deleteUserByMob()
    {
        $mobilenumber = isset($_POST['mobilenumber']) ? trim($_POST['mobilenumber']) : '';
        if (!$mobilenumber) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobilenumber is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->result = $this->Login_model->deleteUserByMob($mobilenumber);

            if ($this->result) {
                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'message' => $this->result->message));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobilenumber not found'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function loginWithOtp()
    {
        $json_obj = $this->readJson();

        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        if (!$mobileNumber) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobileNumber is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {

            $data = $this->Login_model->checkMobile($mobileNumber);
            $err = isset($data['error']) ? $data['error'] : '';

            if (!$err) {

                $this->json_output($data['status'], array('status' => $data['status'], 'message' => $data['message']));
            } else {
                $this->json_output(Created, array('status' => Created, 'message' => ' Otp Generated'));
            }
        }
    }

    public function loginWithPassword()
    {
        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';


        if ($password) {
            $password = $password;
        } else {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'password is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if ($mobileNumber && $password) {
            $this->result =   $this->result = $this->Login_model->loginWithPassword($mobileNumber, $password);

            if (isset($this->result->userId)) {
                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'userId' => $this->result->userId, 'message' =>  $this->result->message));
            } else {
                // $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobilenumber not found'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $this->result->message));
            }
        }
    }


    public function signupGoogle()
    {

        $email = isset($_POST['email']) ? trim($_POST['email']) : '';
        $name = isset($_POST['name']) ? trim($_POST['name']) : '';

        if ($email) {
            $email = $email;
        } else if (!$email) {
            $this->validEmail($email);
        }

        if ($email) {
            $this->result =   $this->result = $this->Login_model->signupGoogle($email);

            if (isset($this->result->userId)) {
                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'userId' => $this->result->userId, 'message' =>  $this->result->message, 'isActive' => $this->result->isActive,));
            } else {

                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'userId' => $this->result->userId, 'message' =>  $this->result->message));
            }
        }
    }

    public function forgotPassword()
    {
        $mobileNumber = isset($_POST['mobileNumber']) ? trim($_POST['mobileNumber']) : '';
        if (!$mobileNumber) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobileNumber is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $this->result =    $this->Login_model->forgotPassword($mobileNumber);
            if (isset($this->result->userId)) {
                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode, 'userId' => $this->result->userId, 'message' =>  $this->result->message, 'isActive' => $this->result->isActive));
            } else {

                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $this->result->message));
            }
        }
    }

    public function updatePassword()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';

        $password = isset($_POST['password']) ? trim($_POST['password']) : '';
        $c_password = isset($_POST['c_password']) ? trim($_POST['c_password']) : '';

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $userId = $userId;
        }



        if (!$password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'password is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else if (!$c_password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'confirm password is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else if ($password != $c_password) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => ' password mismatch'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if ($userId  && $password && $c_password && $password == $c_password) {
            $this->result =  $this->Login_model->updatePassword($userId, $c_password);
            if (isset($this->result)) {
                $this->json_output($this->result->statusCode, array('status' => $this->result->statusCode,  'message' =>  $this->result->message,));
            } else {

                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $this->result->message));
            }
        }
    }

    public function signupFb()
    {

        $json_obj = $this->readJson();

        $facebookId = isset($json_obj->facebookId) ? trim($json_obj->facebookId) : '';
        $name = isset($json_obj->name) ? trim($json_obj->name) : '';
        $emailId = isset($json_obj->emailId) ? trim($json_obj->emailId) : '';
        $mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber) : '';
        $image = isset($json_obj->image) ? trim($json_obj->image) : '';
        if (!$facebookId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'facebookId is blank'];
            $this->jsonOutput($message);
        }
        if (!$name) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'name is blank'];
            $this->jsonOutput($message);
        }
        if (!$emailId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'emailId is blank'];
            $this->jsonOutput($message);
        }

        if (!$mobileNumber) {
            $this->result = $this->Login_model->signupFacebook($facebookId, $name, $emailId, $image);
            $this->jsonOutput(['query' =>  $this->result]);
        } else {
            $this->result = $this->Login_model->signupFacebookWithMob($facebookId, $name, $emailId, $mobileNumber, $image);
            $this->jsonOutput(['query' =>  $this->result]);
        }
    }

    public function test()
    {
        $this->json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        //$this->format_$this->json_output();
    }

    // function $this->json_output($statusHeader, $response)
    // {
    //     $ci = &get_instance();
    //     $ci->output->set_content_type('application/json');
    //     $ci->output->set_status_header($statusHeader);
    //     $ci->output->set_output(json_encode($response));
    // }
}
