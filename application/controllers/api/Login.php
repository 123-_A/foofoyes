<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function login()
    {

        $json_obj = $this->readJson();
        $mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber) : '';


        if (!$mobileNumber) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'mobileNumber is blank']);
        }


        $this->result = $this->Login_model->login($mobileNumber);
        $this->jsonOutput($this->result);
    }





    public function validateOtp()
    {

        $json_obj = $this->readJson();
        $mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber) : '';
        $otp = isset($json_obj->otp) ? trim($json_obj->otp) : '';

        if (!$mobileNumber) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'mobileNumber is blank']);
        }
        if (!$otp) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'otp is blank']);
        }
        $this->result = $this->Login_model->validateOtp($mobileNumber, $otp);
        $this->jsonOutput($this->result);
    }


    public function signup()
    {

        $json_obj = $this->readJson();
        $id = isset($json_obj->id) ? trim($json_obj->id) : '';
        $name = isset($json_obj->name) ? trim($json_obj->name) : '';
        $businessName = isset($json_obj->businessName) ? trim($json_obj->businessName) : '';
        $categories = $json_obj->categories;
        $location = isset($json_obj->location) ? trim($json_obj->location) : '';
        $isPickupAvailable = isset($json_obj->isPickupAvailable) ? trim($json_obj->isPickupAvailable) : '';
        $isFreelancer = isset($json_obj->isFreelancer) ? trim($json_obj->isFreelancer) : '';

        if (!$id) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'id is blank']);
        }

        if (!$name) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'name is blank']);
        }
        if (!$businessName) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'businessName is blank']);
        }
        if (!$location) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'location is blank']);
        }
        $this->result = $this->Login_model->signup($name, $businessName, $id, $categories, $location, $isPickupAvailable, $isFreelancer);
        $this->jsonOutput($this->result);
    }

    public function addCustomer()
    {

        $json_obj = $this->readJson();
        $name = isset($json_obj->name) ? trim($json_obj->name) : '';
        $emailId = isset($json_obj->emailId) ? trim($json_obj->emailId) : '';
        $mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber) : '';
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';

        if (!$mobileNumber) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'mobileNumber is blank']);
        }
        if (!$name) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'name is blank']);
        }
        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }

        $this->result = $this->Login_model->addCustomer($mobileNumber, $name, $userId, $emailId);
        $this->jsonOutput($this->result);
    }

    public function listCustomer()
    {
        $json_obj = $this->readJson();
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';

        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }

        $this->result = $this->Login_model->listCustomer($userId);
        $this->jsonOutput($this->result);
    }
    public function listItem()
    {

        $json_obj = $this->readJson();
        $custId = isset($json_obj->custId) ? trim($json_obj->custId) : '';
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';

        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }

        if (!$custId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'custId is blank']);
        }
        $this->result = $this->Login_model->listItem($custId, $userId);
        $this->jsonOutput($this->result);
    }

    public function getItemMeasurement()
    {

        $json_obj = $this->readJson();
        $itemId = isset($json_obj->itemId) ? trim($json_obj->itemId) : '';


        if (!$itemId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'itemId is blank']);
        }

        $this->result = $this->Login_model->getItemMeasurement($itemId);
        $this->jsonOutput($this->result);
    }
    public function createItemVendor()
    {

        $json_obj = $this->readJson();
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';
        $itemId = isset($json_obj->itemId) ? trim($json_obj->itemId) : '';
        $measId = isset($json_obj->measId) ? trim($json_obj->measId) : '';
        $aliasName = isset($json_obj->aliasName) ? trim($json_obj->aliasName) : '';



        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }
        if (!$itemId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'itemId is blank']);
        }
        if (!$measId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'measId is blank']);
        }
        if (!$aliasName) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'aliasName is blank']);
        }

        $this->result = $this->Login_model->createItemVendor($userId, $measId, $aliasName, $itemId);
        $this->jsonOutput($this->result);
    }

    public function updateItemVendor()
    {
        $json_obj = $this->readJson();
        $itemId = isset($json_obj->itemId) ? trim($json_obj->itemId) : '';
        $tax = isset($json_obj->tax) ? trim($json_obj->tax) : '';
        $rate = isset($json_obj->rate) ? trim($json_obj->rate) : '';

        if (!$itemId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'itemId  is blank']);
        }
        if (!$tax) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'tax is blank']);
        }
        if (!$rate) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'rate is blank']);
        }

        $this->result = $this->Login_model->updateItemVendor($itemId, $tax, $rate);
        $this->jsonOutput($this->result);
    }
    public function listItemVendor()
    {

        $json_obj = $this->readJson();
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';
        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }

        $this->result = $this->Login_model->listItemVendor($userId);
        $this->jsonOutput($this->result);
    }
    public function listWork()
    {

        $json_obj = $this->readJson();
        $this->result = $this->Login_model->listWork();
        $this->jsonOutput($this->result);
    }

    public function createCustomerMeasurementOrderDetail()
    {

        $json_obj = $this->readJson();

        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';
        //$itemId = isset($json_obj->itemId) ? trim($json_obj->itemId): '';
        $orderDetail = $json_obj->orderDetail;
        $custId = isset($json_obj->custId) ? trim($json_obj->custId) : '';
        //$measurements = $json_obj->measurements;
        //$work = $json_obj->work;
        //$deliveryDate = isset($json_obj->deliveryDate) ? trim($json_obj->deliveryDate): '';


        //$voiceInstruction = isset($json_obj->voiceInstruction) ? trim($json_obj->voiceInstruction): '';
        //$description = isset($json_obj->description) ? trim($json_obj->description): '';
        //$status = isset($json_obj->status) ? trim($json_obj->status): '';




        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }


        if (!$custId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'custId is blank']);
        }

        //if( !$deliveryDate ){
        //$this->jsonOutput(['status'=> 'fail', 'message' => 'deliveryDate is blank']);     
        //  }

        //if( !$itemId){
        //$this->jsonOutput(['status'=> 'fail', 'message' => 'itemId is blank']);     
        // }	

        //if( !$sampleDress){
        //$this->jsonOutput(['status'=> 'fail', 'message' => 'sampleDress is blank']);     
        // }	


        $this->result = $this->Login_model->createCustomerMeasurementOrderDetail($userId, $custId, $orderDetail);
        $this->jsonOutput($this->result);
    }

    public function listOrder()
    {
        $json_obj = $this->readJson();
        $orderId = isset($json_obj->orderId) ? trim($json_obj->orderId) : '';
        if (!$orderId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'orderId is blank']);
        }

        $this->result = $this->Login_model->listOrder($orderId);
        $this->jsonOutput($this->result);
    }
    public function payment()
    {
        $json_obj = $this->readJson();
        $orderId = isset($json_obj->orderId) ? trim($json_obj->orderId) : '';

        $work = $json_obj->work;
        $itemId = isset($json_obj->itemId) ? trim($json_obj->itemId) : '';


        if (!$orderId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'orderId is blank']);
        }

        if (!$itemId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'itemId  is blank']);
        }

        $this->result = $this->Login_model->payment($orderId, $work, $itemId);
        $this->jsonOutput($this->result);
    }
    public function listPayment()
    {
        $json_obj = $this->readJson();
        $orderId = isset($json_obj->orderId) ? trim($json_obj->orderId) : '';
        $custId = isset($json_obj->custId) ? trim($json_obj->custId) : '';

        if (!$orderId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'orderId is blank']);
        }
        if (!$custId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'custId is blank']);
        }

        $this->result = $this->Login_model->listPayment($orderId, $custId);
        $this->jsonOutput($this->result);
    }
    public function listJobConfirmation()
    {
        $json_obj = $this->readJson();

        $this->result = $this->Login_model->listJobConfirmation();
        $this->jsonOutput($this->result);
    }
    public function getjobcount()
    {
        $json_obj = $this->readJson();

        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';


        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }


        $this->result = $this->Login_model->getjobcount($userId);
        $this->jsonOutput($this->result);
    }
    public function  listJobItems()
    {
        $json_obj = $this->readJson();

        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';
        $jobTableId = isset($json_obj->jobTableId) ? trim($json_obj->jobTableId) : '';


        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }
        if (!$jobTableId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'jobTableId is blank']);
        }


        $this->result = $this->Login_model->listJobItems($userId, $jobTableId);
        $this->jsonOutput($this->result);
    }

    public function listJobDetails()
    {
        $json_obj = $this->readJson();

        //$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
        $orderId = isset($json_obj->orderId) ? trim($json_obj->orderId) : '';


        //if( !$userId ){
        //$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        // } 
        if (!$orderId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'orderId is blank']);
        }


        $this->result = $this->Login_model->listJobDetails($orderId);
        $this->jsonOutput($this->result);
    }



    public function test()
    {
        $json_obj = $this->readJson();

        $orderId = isset($json_obj->orderId) ? trim($json_obj->orderId) : '';



        if (!$orderId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'orderId is blank']);
        }



        $this->result = $this->Login_model->test($orderId);
        $this->jsonOutput($this->result);
    }
    public function  createGalleryFolder()
    {
        $json_obj = $this->readJson();

        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';

        $folderName = isset($json_obj->folderName) ? trim($json_obj->folderName) : '';

        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }
        if (!$folderName) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'folderName is blank']);
        }




        $this->result = $this->Login_model->createGalleryFolder($userId, $folderName);
        $this->jsonOutput($this->result);
    }




    public function  listGalleryFolder()
    {
        $json_obj = $this->readJson();

        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';



        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }



        $this->result = $this->Login_model->listGalleryFolder($userId);
        $this->jsonOutput($this->result);
    }

    public function  createGalleryDetails()
    {

        $json_obj = $this->readJson();

        $galleryTableId = isset($json_obj->galleryTableId) ? trim($json_obj->galleryTableId) : '';

        $imgUrl = isset($json_obj->imgUrl) ? trim($json_obj->imgUrl) : '';

        if (!$galleryTableId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'galleryTableId is blank']);
        }
        if (!$imgUrl) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'imgUrl is blank']);
        }




        $this->result = $this->Login_model->createGalleryDetails($galleryTableId, $imgUrl);
        $this->jsonOutput($this->result);
    }


    public function  uploadImage()
    {
        $this->load->library('s3_upload');
        $json_obj = $this->readJson();

        $galleryTableId = isset($json_obj->galleryTableId) ? trim($json_obj->galleryTableId) : '';
        $userId = isset($json_obj->userId) ? trim($json_obj->userId) : '';

        $productImage = isset($json_obj->productImage) ? trim($json_obj->productImage) : '';

        if (!$galleryTableId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'galleryTableId is blank']);
        }
        if (!$productImage) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'productImage is blank']);
        }
        if (!$userId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'userId is blank']);
        }





        $this->result = $this->Login_model->uploadImage($galleryTableId, $productImage, $userId);
        $this->jsonOutput($this->result);
    }
    public function  listGalleryDetails()
    {
        $json_obj = $this->readJson();

        $galleryTableId = isset($json_obj->galleryTableId) ? trim($json_obj->galleryTableId) : '';



        if (!$galleryTableId) {
            $this->jsonOutput(['status' => 'fail', 'message' => 'galleryTableId is blank']);
        }



        $this->result = $this->Login_model->listGalleryDetails($galleryTableId);
        $this->jsonOutput($this->result);
    }
    public function addUserFirebaseDetails()
    {
        $userId =  $this->input->post('userId');
        $token =  $this->input->post('token');
        $deviceId =  $this->input->post('deviceId');
        $deviceType =  $this->input->post('deviceType');

        if (!$userId) {
            $message = ['status' => 'fail', 'message' => 'userId is blank'];

            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$token) {
            $message = ['status' => 'fail', 'message' => 'token is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$deviceId) {
            $message = ['status' => 'fail', 'message' => 'deviceId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$deviceType) {
            $message = ['status' => 'fail', 'message' => 'deviceType is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($token) && ($deviceId) && ($deviceType)) {
            $this->result = $this->Login_model->addUserFirebaseDetails($userId, $token, $deviceId, $deviceType);
            $this->json_output(Created, array('status' => Created, 'message' =>  $message));
        }
    }
    public function deleteUserFirebaseDetails()
    {



        $userId =  $this->input->post('userId');

        $deviceId =  $this->input->post('deviceId');
        if (!$userId) {
            $message = ['status' => 'fail', 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$deviceId) {
            $message = ['status' => 'fail', 'message' => 'deviceId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (($userId)  && ($deviceId)) {
            $this->result = $this->Login_model->deleteUserFirebaseDetails($userId, $deviceId);
            $this->json_output(Successfull, array('status' => Successfull, 'message' =>  $message));
        }
    }
}
