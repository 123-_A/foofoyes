<?php
defined('BASEPATH') or exit('No direct script access allowed');

class City extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('admin/city_model', 'cmodel');

        $this->is_session_exist();
    }

    public function index()
    {
        $data = array();
        $data['country'] =  $this->cmodel->getAllContry();
        $data['alstates'] =  $this->cmodel->getAllState();
        $data['aldistrict'] =  $this->cmodel->getAlDistrict();
        $data['alcitys'] =  $this->cmodel->getAlcitys();
        $data['country'] =  $this->cmodel->getAllCountrys();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/city/manage',  $data);
        // $this->load->view('admin/layout/footer');

    }
    public function newcountry()
    {


        $this->form_validation->set_rules('country_name', 'Password', 'trim|required');
        $country_name = $this->input->post('country_name');




        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/layout/header');
            $this->load->view('admin/common/sidebar');
            $this->load->view('admin/city/manage');
            $this->load->view('admin/layout/footer');
        } else {
            $resp =   $this->cmodel->newCountry($country_name);

            if (($resp)) {
                $this->session->set_flashdata('success', 'Country added successfully');

                redirect('addcity');
            } else {
                $this->session->set_flashdata('danger', 'Failed to add country');

                redirect('addcity');
            }
        }
    }

    public function newstate()
    {



        $this->form_validation->set_rules('countryId', 'Country name', 'trim|required');
        $this->form_validation->set_rules('stname', 'State name', 'trim|required');
        $countryid = $this->input->post('countryId');
        $stname = $this->input->post('stname');




        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/layout/header');
            $this->load->view('admin/common/sidebar');
            $this->load->view('admin/city/manage');
            $this->load->view('admin/layout/footer');
        } else {

            $resp =   $this->cmodel->newState($countryid, $stname);

            if (($resp)) {
                $this->session->set_flashdata('success', 'State added successfully');

                redirect('addcity');
            } else {
                $this->session->set_flashdata('danger', 'Failed to add State');

                redirect('addcity');
            }
        }
    }

    public function newdistrict()
    {



        $this->form_validation->set_rules('countryId', 'Country name', 'trim|required');
        $this->form_validation->set_rules('stateId', 'State name', 'trim|required');
        $this->form_validation->set_rules('district', 'District name', 'trim|required');
        $countryid = $this->input->post('countryId');
        $stateId = $this->input->post('stateId');
        $district = $this->input->post('district');




        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/layout/header');
            $this->load->view('admin/common/sidebar');
            $this->load->view('admin/city/manage');
            $this->load->view('admin/layout/footer');
        } else {

            $resp =   $this->cmodel->newDistrict($countryid, $stateId, $district);

            if (($resp)) {
                $this->session->set_flashdata('success', 'District added successfully');

                redirect('addcity');
            } else {
                $this->session->set_flashdata('danger', 'Failed to add District');

                redirect('addcity');
            }
        }
    }

    public function newdcity()
    {



        $this->form_validation->set_rules('countryId', 'Country name', 'trim|required');
        $this->form_validation->set_rules('stateId', 'State name', 'trim|required');
        $this->form_validation->set_rules('districtId', 'District name', 'trim|required');
        $this->form_validation->set_rules('cityname', 'City name', 'trim|required');
        $countryid = $this->input->post('countryId');
        $stateId = $this->input->post('stateId');
        $districtId = $this->input->post('districtId');
        $city = $this->input->post('cityname');




        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/layout/header');
            $this->load->view('admin/common/sidebar');
            $this->load->view('admin/city/manage');
            $this->load->view('admin/layout/footer');
        } else {

            $resp =   $this->cmodel->newCity($countryid, $stateId, $districtId, $city);

            if (($resp)) {
                $this->session->set_flashdata('success', 'City added successfully');

                redirect('addcity');
            } else {
                $this->session->set_flashdata('danger', 'Failed to add City');

                redirect('addcity');
            }
        }
    }

    public function getstatebyid()
    {
        $cntryid =  $this->input->post('countryid');
        $resp =   $this->cmodel->getStateNameByCntryId($cntryid);
        echo json_encode($resp);
    }
    public function getdistrictbyid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   $this->cmodel->getCountryNameById($cityid);
        echo json_encode($resp);
    }

    public function deletecntry($id)
    {
        $this->cmodel->deletecntry($id);
        $this->session->set_flashdata('success', 'Country deleted  successfully');

        redirect('addcity');
    }
}
