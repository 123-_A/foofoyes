<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Delivery_partner extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/Delivery_partner_model', 'dpmodel');
    }

    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $data['allpartners'] = $this->dpmodel->getAlldeliveryPartners();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/dpartner/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function getstatebyid()
    {
        $cntryid =  $this->input->post('countryid');
        $resp =  getStateNameByCntryId($cntryid);
        echo json_encode($resp);
    }
    public function getdistrictbyid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCountryNameById($cityid);
        echo json_encode($resp);
    }
    public function getcitiesbydid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCityNameByDistrictId($cityid);
        echo json_encode($resp);
    }
    public function newpartner()
    {

        $partner['partner_name'] = $this->input->post('yourname');
        $partner['contact_no'] = $this->input->post('contactno');
        $partner['address'] = $this->input->post('address');
        $partner['dob'] = $this->input->post('dob');
        $partner['country_id'] = $this->input->post('countryId');
        $partner['state_id'] = $this->input->post('stateid');
        $partner['destrict_id'] = $this->input->post('districtId');
        $partner['city_id'] = $this->input->post('cityId');
        $partner['picnode'] = $this->input->post('pincode');
        $partner['email'] = $this->input->post('email');
        $partner['working_preference'] = $this->input->post('working_pref');
        $partner['driving_licence'] = $this->input->post('licenceno');
        $partner['blood_group'] = $this->input->post('bldgrp');
        $partner['prefered_working_location'] = $this->input->post('pref_loc');
        $partner['joining_date'] = $this->input->post('jod');
        $partner['latitude'] = $this->input->post('lat');
        $partner['longitude'] = $this->input->post('long');
        $uname  = $this->input->post('uname');
        $pass = $this->input->post('password');
        $latti = $this->input->post('lat');
        $longi = $this->input->post('long');


        $config['upload_path'] = 'assets/sadmin/deliverypartner';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';

        $this->load->library('upload', $config);


        $this->upload->initialize($config);

        if (!$this->upload->do_upload('insrendingdate')) {
            $error = array('error' => $this->upload->display_errors());
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' =>  $error];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $data['upload'] =  $this->upload->data();
            $fname = $data['upload']['file_name'];
            $partner['insurance_ending'] = 'assets/sadmin/deliverypartner' . $fname;
        }
        $res =   $this->dpmodel->addNewDelPartner($partner, $uname, $pass, $latti, $longi);
        echo json_encode($res);
    }
}
