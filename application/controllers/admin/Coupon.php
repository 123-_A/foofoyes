<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coupon extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/coupon_model', 'cmodel');
    }
    public function index()
    {
        $data = [];
        // $data['country'] = loadcountry();
        // $data['ufiles'] = $this->rmodel->getAllPosts();
        $data['allPoints'] = $this->cmodel->getPoints();
        $data['settings'] = $this->cmodel->getSettings();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/coupon/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function geteditdata()
    {
        $id = $this->input->post('pId');
        $resp =  $this->cmodel->geteditdata($id);
        echo json_encode($resp);
    }
    public function deleteposts()
    {
        $id = $this->input->post('id');
        $resp =  $this->cmodel->deleteposts($id);
        echo json_encode($resp);
    }
    public function addpointsval()
    {
        $_vId = $this->input->post('_vId');
        $_froms = $this->input->post('_froms');
        $_tos = $this->input->post('_tos');
        $_pnts =  $this->input->post('_pnts');
        $resp =  $this->cmodel->addNewPoints($_froms, $_tos, $_pnts, $_vId);
        echo json_encode($resp);
    }
    public function deletepoints()
    {
        $_vId = $this->input->post('_vId');
        $resp =  $this->cmodel->deletepoints($_vId);
        echo json_encode($resp);
    }
    public function updatepointvalue()
    {
        $_vId = $this->input->post('_vId');
        $resp =  $this->cmodel->updatepointvalue($_vId);
        echo json_encode($resp);
    }
    public function updateminimumpnt()
    {
        $_vId = $this->input->post('_vId');
        $resp =  $this->cmodel->updateminimumpnt($_vId);
        echo json_encode($resp);
    }
    public function updateorderamount()
    {
        $_vId = $this->input->post('_vId');
        $resp =  $this->cmodel->updateorderamount($_vId);
        echo json_encode($resp);
    }
}
