<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('res/order_model', 'omodel');
        $this->is_res_session_exist();
    }

    public function order($offset = 0)
    {

        $data = array();
        $this->load->library('pagination');
        $config['base_url'] = base_url('restaurant/order/order');
        $config['total_rows'] = $this->omodel->getnumOfRecords();
        $config['per_page'] = 5;
        //pagination styles start
        $config['full_tag_open'] = "<ul class='pagination newclasstart'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="nearby_page">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active active_pagination"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="test7">';
        $config['prev_tag_open'] = '<div class="previous newclasstart">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="test 2">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="test 1">';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = '<i class="fa fa-long-arrow-left previouspagilink"></i>Previous Page';
        $config['prev_tag_open'] = '<li class="first_page">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right nextpagilink"></i>';
        $config['next_tag_open'] = '<li class="nxt_pge">';
        $config['next_tag_close'] = '</div>';
        $config['next_tag_close'] = '</li>';

        // pagination style end
        $this->pagination->initialize($config);
        $data = array();
        $data['listorders'] =   $this->omodel->getMyOrders($config['per_page'], $offset);



        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/order/home', $data);
        // $this->load->view('restaurant/layout/footer');
    }

    public function orderSearch($offset = 0)
    {
        $fdnme = $this->input->post('fudname');
        // $data = array();
        // $this->load->library('pagination');
        // $config['base_url'] = base_url('restaurant/order/order');
        // $config['total_rows'] = $this->omodel->getnumOfRecordsSrch($fdnme);
        // $config['per_page'] = 5;
        // //pagination styles start
        // $config['full_tag_open'] = "<ul class='pagination newclasstart'>";
        // $config['full_tag_close'] = '</ul>';
        // $config['num_tag_open'] = '<li class="nearby_page">';
        // $config['num_tag_close'] = '</li>';
        // $config['cur_tag_open'] = '<li class="active active_pagination"><a href="#">';
        // $config['cur_tag_close'] = '</a></li>';
        // $config['prev_tag_open'] = '<li class="test7">';
        // $config['prev_tag_open'] = '<div class="previous newclasstart">';
        // $config['prev_tag_close'] = '</li>';
        // $config['first_tag_open'] = '<li class="test 2">';
        // $config['first_tag_close'] = '</li>';
        // $config['last_tag_open'] = '<li class="test 1">';
        // $config['last_tag_close'] = '</li>';



        // $config['prev_link'] = '<i class="fa fa-long-arrow-left previouspagilink"></i>Previous Page';
        // $config['prev_tag_open'] = '<li class="first_page">';
        // $config['prev_tag_close'] = '</li>';


        // $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right nextpagilink"></i>';
        // $config['next_tag_open'] = '<li class="nxt_pge">';
        // $config['next_tag_close'] = '</div>';
        // $config['next_tag_close'] = '</li>';

        // pagination style end
        // $this->pagination->initialize($config);
        $data = array();
        $data['listorders'] =   $this->omodel->getMyOrdersSearch($fdnme);



        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/order/homesearch', $data);
        // $this->load->view('restaurant/layout/footer');
    }

    public function updateorderstatus()
    {
        $orderid = $this->input->post('orderid');
        $status = $this->input->post('status');
        $resp = $this->omodel->updateorderstatus($orderid, $status);
        echo json_encode($resp);
    }

    public function viewOrder($id)
    {
        $data = [];
        $id = base64_decode($id);
        $data['orders'] =  $this->omodel->getOrderById($id);
        if ($data['orders']['orderdetails']->payment_method != 'COD') {
            $data['bankdetails'] =   $this->omodel->getBankDetails($id, $data['orders']['orderdetails']->deliveryMode, $data['orders']['orderdetails']->time_slot);
        }

        $data['allStatus'] =  $this->omodel->getOrderStatus();

        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/order/vieworder', $data);
    }

    public function cancelorder()
    {
        $orderid = $this->input->post('orderid');
        $status = $this->input->post('status');
        $reason = $this->input->post('reason');
        $resp = $this->omodel->cancelorder($orderid, $status, $reason);
        echo json_encode($resp);
    }
}
