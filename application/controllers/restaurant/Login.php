<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('res/login_model', 'lgmodel');
        $this->resloginChk();
    }

    public function index()
    {

        $this->load->view('restaurant/login/login');
        $this->res_is_loggedin();
    }
    public function loginCheck()
    {
        $this->form_validation->set_rules('partnername', 'Partner Name', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $partnername = $this->input->post('partnername');
        $pas = $this->input->post('password');


        if ($this->form_validation->run() == FALSE) {

            $this->load->view('restaurant/login/login');
        } else {
            $resp =   $this->lgmodel->checkPartnerExist($partnername, $pas);

            if (($resp) && ($resp != 'null')) {
                $user_data = array(
                    'uid'  => $resp['partners_id'],
                    'id'     => $resp['id'],
                    'username' => $resp['username'],
                    'partner_type' => $resp['partner_type'],
                    'longitude' => $resp['longitude'],
                    'latitude' => $resp['latitude'],
                    'is_active' => $resp['is_active'],
                );

                $this->session->set_userdata($user_data);
                $this->res_is_loggedin();
            } else {
                $this->session->set_flashdata('danger', 'invalid login credentials');

                redirect('restaurant');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('restaurant');
    }
}
